package main.TPListasOrdenadas;

import struct.impl.DynamicSortedList;

import java.io.*;

/**
 * class TransportationSecretary
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/11/2017.
 * <p>
 *     This class simulates a secretary of transportation which manages buses.
 * </p>
 */
public class TransportationSecretary {
    private DynamicSortedList<Bus> buses;

    public TransportationSecretary() {
        buses = new DynamicSortedList<>();
    }

    public void addBus(Bus bus){
        buses.insert(bus);
    }

    public void removeBus(int line, int internal){
        buses.goTo(0);
        Bus bus = new Bus(line, internal, 0, false);
        if (buses.size() == 0) System.out.println("There are no buses");
        for (int i=0; i<buses.size(); i++){
            buses.goTo(i);
            if (bus.compareTo(buses.getActual()) == 0){
                buses.remove();
                System.out.println("Bus removed successfully!");
                return;
            } if (bus.compareTo(buses.getActual()) < 0){
                System.out.println("Bus does not exist");
                return;
            }
        }
        System.out.println("Bus does not exist");
    }

    /**
     *Prints a report of the buses ordered by line number and then internal number
     */
    public void print(){
        for (int i=0; i<buses.size();i++){
            buses.goTo(i);
            Bus bus1 = buses.getActual();
            System.out.println("lineNumber: " + bus1.getLineNumber() +"\t"+"internalNumber: " + bus1.getInternalNumber());
        }
    }

    /**
     * Prints a report of the amount of disabled-friendly buses in any line
     */
    public void disabledIntel(){
        if (buses.size() == 0){
            System.out.println("There are currently no buses to display.");
            return;
        }
        int counter = 0;
        buses.goTo(0); //changed 0 for 1
        int line = buses.getActual().getLineNumber();
        for (int i=0; i<buses.size();i++){ //changed 0 for 1
            buses.goTo(i);
            if (line!=buses.getActual().getLineNumber()){
                System.out.println("Amount of disabled friendly buses in line "+line+": "+counter);
                line = buses.getActual().getLineNumber();
                counter=0;
            }
            if (buses.getActual().isDisabledFriendly()) counter++;
        }
        System.out.println("Amount of disabled friendly buses in line "+line+": "+counter);
    }

    /**
     * Prints a report of the amount of buses with more than 27 seats in any line.
     */
    public void seatsInLine(){
        if (buses.size() == 0){
            System.out.println("There are currently no buses to display.");
            return;
        }
        int counter = 0;
        buses.goTo(0); //changed 0 for 1
        int line = buses.getActual().getLineNumber();
        for (int i=0; i<buses.size();i++){ //changed 0 for 1
            buses.goTo(i);
            if (line!=buses.getActual().getLineNumber()){
                System.out.println("Amount of buses with more than 27 seats in line "+line+": "+counter);
                line = buses.getActual().getLineNumber();
                counter=0;
            }
            if (buses.getActual().getNumberOfSeats()>27) counter++;
        }
        System.out.println("Amount of buses with more than 27 seats in line "+line+": "+counter);
    }

    /**
     * Saves the list of buses in an archive
     */
    public void saveBuses(){
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;

        try {

            fout = new FileOutputStream("src//main//TPListasOrdenadas//buses");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(buses);

            System.out.println("Done");

        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {

            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * Reads the list of buses in an archive.
     * @return the list of buses.
     */
    public DynamicSortedList<Bus> retrieveBuses(){
        DynamicSortedList address = null;

        FileInputStream fin = null;
        ObjectInputStream ois = null;

        try {

            fin = new FileInputStream("src//main//TPListasOrdenadas//buses");
            ois = new ObjectInputStream(fin);
            address = (DynamicSortedList) ois.readObject();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return address;
    }
}
