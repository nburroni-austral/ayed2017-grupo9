package struct.impl;

/**
 * class StaticQueue
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/9/2017.
 * <p>
 *     This class is an implementation of Queue using static memory.
 * </p>
 */
public class StaticQueue<T> {

    private int size;
    private int capacity;
    private int front;
    private int back;
    private Object[] data;

    public StaticQueue(int length){
        capacity = length;
        size = 0;
        front = 0;
        back = 0;
        data = new Object[length];
    }

    public void inqueue(T o){
        if (size < capacity && back < capacity) {
            data[back++] = o;
            size++;
        } else {
            if (back==capacity && size < capacity){
                back = 0;
                data[back++] = o;
                size++;
            } else {
                grow();
                data[back++] = o;
                size++;
            }
        }
    }

    public T dequeue(){
        if (!isEmpty()){
            size--;
            return (T) data[front++];
        }
        throw new RuntimeException("No items dude.");
    }

    public boolean isEmpty(){
        return size==0;
    }

    public int length(){
        return capacity;
    }

    public int size(){
        return size;
    }

    public void empty(){
        front = 0;
        back = 0;
    }

    private void grow(){
        Object[] data2 = new Object[capacity*2];
        int j = 0;
        for (int i=front; i<=size; i++){
            if (i==back && j>=size-front) break;
            else if (i==size) i = 0;
            data2[j] = data[i];
            j++;
        }
        capacity = capacity*2;
        data = data2;
        front = 0;
        back = size;
    }
}
