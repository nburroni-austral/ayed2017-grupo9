package main.TPSearchBinTree;

/**
 * Created by Gianni on 4/19/2017.
 */
public class Element<T> implements Comparable<Element<T>> {//may not be needed
    private int key;
    private T data;

    public Element(int key, T data) {
        this.key = key;
        this.data=data;
    }

    @Override
    public int compareTo(Element<T> o) {
        return key-o.getKey();
    }

    public int getKey() {
        return key;
    }

    public T getData() {
        return data;
    }
}
