package main.TPArbolesBinarios;

import java.io.Serializable;

/**
 * Created by Gianni on 6/19/2017.
 */
public class IndexElement implements Comparable<IndexElement>, Serializable{
    private int dni;
    private int index;

    public IndexElement(int dni, int index) {
        this.dni = dni;
        this.index = index;
    }

    @Override
    public int compareTo(IndexElement o) {
        return dni-o.getDni();
    }

    public int getDni() {
        return dni;
    }

    public int getIndex() {
        return index;
    }
}
