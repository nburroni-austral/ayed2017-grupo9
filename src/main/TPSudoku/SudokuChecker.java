package main.TPSudoku;

/**
 * class SudokuChecker
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/4/17.
 * <p>
 *     Checks if a Sudoku board provided by the user is valid, by checking if it violates any rules,
 *     such as a number repeated in a column, row or square.
 * </p>
 */
public class SudokuChecker {

    public SudokuChecker(){
    }

    /**
     * Checks if a Sudoku board is valid.
     * @param board The board to be checked, represented as a two-dimensional array of Integers.
     * @return True if the board is valid, false otherwise.
     */
    public boolean check(Integer[][] board){
        for(int i=0; i<9; i++){
            for(int j=0; j<9; j++){
                if(!(testSameRow(board, i, j) && testSameColumn(board, i, j) && testSameSquare(board, i, j))){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean testSameRow(Integer[][] board, int x, int y){
        for (int i = 0; i<9; i++){
            try{
                if ((board[x][i].equals(board[x][y])) && (y!=i)) return false;
            } catch (NullPointerException e){
            }
        }
        return true;
    }

    private boolean testSameColumn(Integer[][] board, int x, int y){
        for (int i = 0; i<9; i++){
            try{
                if ((board[i][y].equals(board[x][y])) && (x!=i)) return false;
            } catch (NullPointerException e){
            }
        }
        return true;
    }

    private boolean testSameSquare(Integer[][] board, int x, int y){
        int xSquare = xPositionSquare(x);
        int ySquare = yPositionSquare(y);
        for (int i = xSquare; i<xSquare+3; i++){
            for (int j = ySquare; j<ySquare+3; j++){
                try{
                    if ((board[i][j].equals(board[x][y])) && (x!=i) && (y!=j)) return false;
                } catch (NullPointerException e){
                }
            }
        }
        return true;
    }

    private int xPositionSquare(int x){
        if (x < 3) return 0;
        else if (x < 6) return 3;
        else return 6;
    }

    private int yPositionSquare(int y){
        if (y < 3) return 0;
        else if (y < 6) return 3;
        else return 6;
    }
}
