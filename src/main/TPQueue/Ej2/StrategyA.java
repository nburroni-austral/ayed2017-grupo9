package main.TPQueue.Ej2;

import struct.impl.DynamicQueue;

/**
 * class StrategyA
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/11/17.
 * <p>
 *     MainBank is used for testing purposes for Bank and all its related classes. This specific implementation
 *     if for a bank in which there are three queues, each for a different cashier.
 * </p>
 */
public class StrategyA implements Strategy{

    private int peopleWaiting;
    private int interval;

    public StrategyA(int interval){
        this.interval = interval;
        peopleWaiting = 0;
    }

    /**
     * Sorts an entering batch of clients into queues.
     * @param clients The batch of clients
     * @param cashiers The bank's cashiers
     */
    public void sortClients(Client[] clients, Cashier[] cashiers){
        for (int i=0; i<clients.length; i++){
            if (clients[i].leaves(peopleWaiting));
            else fillQueue(clients[i], cashiers);
        }
        startEmptyCashiers(cashiers);
    }

    /**
     * Updates cashiers, increases time by the unit of time.
     * @param cashiers The bank's cashiers
     */
    public void updateCashiers(Cashier[] cashiers){
        int waiting = 0;
        for (int i=0; i<cashiers.length; i++){
            cashiers[i].nextInterval(interval);
            waiting += cashiers[i].getQueue().size();
        }
        peopleWaiting = waiting;
    }

    public int getPeopleWaiting(){
        return peopleWaiting;
    }

    private void fillQueue(Client client, Cashier[] cashiers){
        int minimumAmount = cashiers[0].getQueue().size();
        int minimumOccurrence = 1;
        for (int j=1; j<cashiers.length; j++){
            if (cashiers[j].getQueue().size() < minimumAmount){
                minimumAmount = cashiers[j].getQueue().size();
                minimumOccurrence = 1;
            }
            if (cashiers[j].getQueue().size() < minimumAmount) minimumOccurrence++;
        }
        DynamicQueue[] shortestQueues = new DynamicQueue[minimumOccurrence];
        int k = 0;
        for (int j=0; j<cashiers.length && k<shortestQueues.length; j++){
            if (cashiers[j].getQueue().size() == minimumAmount){
                shortestQueues[k] = cashiers[j].getQueue();
                k++;
            }
        }
        int chosen = client.chooseQueue(shortestQueues);
        shortestQueues[chosen].enqueue(client);
        peopleWaiting++;
    }

    private void startEmptyCashiers(Cashier[] cashiers){
        for (int i=0; i<cashiers.length; i++){
            if (cashiers[i].isEmpty()){
                cashiers[i].start();
                peopleWaiting--;
            }
        }
    }
}
