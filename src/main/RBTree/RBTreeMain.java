package main.RBTree;

import struct.impl.RBTree.RBTree;

/**
 * Created by Dwape on 5/27/17.
 */
public class RBTreeMain {

    public static void main(String[] args) {

        RBTree<Integer> tree = new RBTree<>();

        int number = Scanner.getInt("Enter an ID (0 to display tree): ");
        while (number != 0){
            tree.insert(number);
            System.out.println("ID added successfully.");
            number = Scanner.getInt("Enter an ID (0 to display tree): ");
        }
        RBTreeDrawer draw = new RBTreeDrawer();
        draw.drawTree(tree);
    }
}
