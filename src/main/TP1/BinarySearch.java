package main.TP1;

/**
 * Created by Dwape on 3/7/17.
 */
public class BinarySearch <T extends Comparable>{

    public BinarySearch(){
    }

    public boolean binarysSearch(T[] a, T k){
        return binarySearch(a, k, 0, a.length-1);
    }
     private boolean binarySearch(T[] a, T k, int first, int last){
         int middle = (last-first)/2+first;
         if(a[middle].compareTo(k) == 0) return true;
         if(first==last) return false;
         else if(a[middle].compareTo(k) == -1){
             return binarySearch(a, k, middle+1, last);
         } else {
             return binarySearch(a, k, first, middle-1);
         }
     }

    public int binarySearchIndex(T[]a, T k){
        return binarySearchIndex(a, k, 0, a.length-1);
    }

    public int binarySearchIndex(T[] a, T k, int first, int last){
        int middle = (last-first)/2+first;
        if(a[middle].compareTo(k) == 0) return middle;
        if(first==last) throw new RuntimeException(); //element not found.
        else if(a[middle].compareTo(k) == -1){
            return binarySearchIndex(a, k, middle+1, last);
        } else {
            return binarySearchIndex(a, k, first, middle-1);
        }
    }
}
