package main.TPHashTable;

import struct.impl.DynamicList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Dwape on 7/6/17.
 */
public class CorrectorWindow extends JFrame{

    JPanel mainPanel;
    JPanel subPanel;
    JPanel suggestionsPanel;
    JPanel controlPanel;
    //JTextField option;

    public CorrectorWindow(ActionListener next){
        setTitle("Corrector Window");
        editLayout(next);
        pack();
        setLocationRelativeTo(null);
    }

    private void editLayout(ActionListener next){
        mainPanel = new JPanel(new BorderLayout());
        subPanel = new JPanel();
        suggestionsPanel = new JPanel();
        controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel,BoxLayout.PAGE_AXIS));
        JButton nextButton = new JButton("next");
        nextButton.addActionListener(next);
        controlPanel.add(new Label("     "));
        controlPanel.add(nextButton);
        controlPanel.add(new Label("      "));
        mainPanel.add(controlPanel, BorderLayout.LINE_END);
        mainPanel.add(subPanel);
        add(mainPanel);
        pack();
    }

    public void showSuggestions(String incorrectWord,DynamicList<Word> suggestions){
        suggestionsPanel.setLayout(new BoxLayout(suggestionsPanel,BoxLayout.PAGE_AXIS));
        JLabel label = new JLabel(" Suggestions for " + incorrectWord +":  ");
        suggestionsPanel.add(label);
        for (int i=1; i<suggestions.size();i++){
            suggestions.goTo(i);
            JLabel label1 = new JLabel(suggestions.getActual().getString());
            suggestionsPanel.add(label1);
        }
        suggestionsPanel.add(new JLabel(" "));
        subPanel.add(suggestionsPanel,BorderLayout.CENTER);
        pack();
    }
}
