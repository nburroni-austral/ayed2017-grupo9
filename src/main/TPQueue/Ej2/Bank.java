package main.TPQueue.Ej2;

/**
 * class Bank
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/11/17.
 * <p>
 *     Simulates the functioning of a bank.
 * </p>
 */
public class Bank {

    private Cashier[] cashiers;
    private Strategy method;
    private int timeInMinutes10;
    private boolean open;
    private int clientsWaiting;
    private int timeBetweenBatches;
    private int interval;
    private int maxPeople;
    private int minPeople;

    public Bank(Strategy method, Cashier[] cashiers){
        this.method = method;
        this.cashiers = cashiers;
        timeBetweenBatches = 15;
        interval = 1;
        maxPeople = 10;
        minPeople = 1;
    }

    /**
     * Begins a work day in the bank, from its opening time until all clients in queues have been attended.
     * @return the number of clients attended
     */
    public int startWorkDay(){
        timeInMinutes10 = 6000;
        open = true;
        while (open || clientsWaiting > 0){
            nextInterval();
        }
        int clientsAttended = 0;
        for (int i=0; i<cashiers.length; i++){
            clientsAttended += cashiers[i].getPeopleAttended();
        }
        return clientsAttended;
    }

    /**
     * Increases time for bank and all cashiers by the minimum unit of time.
     */
    private void nextInterval(){
        method.updateCashiers(cashiers);
        if (timeInMinutes10 < 9000){
            if (timeInMinutes10%timeBetweenBatches == 0){
                method.sortClients(nextBatch(), cashiers);
            }
        } else open = false;
        timeInMinutes10 += interval;
        clientsWaiting = method.getPeopleWaiting();
    }

    /**
     * Creates a new batch of clients with a random amount of people within defined boundaries.
     * @return An array with all the new entering clients
     */
    private Client[] nextBatch(){
        int numberInBatch = (int) (Math.random()*(maxPeople-minPeople)+minPeople);
        Client[] batch = new Client[numberInBatch];
        for (int i=0; i<batch.length; i++){
            batch[i] = new Client();
        }
        return batch;
    }
}
