package main.TP1;

/**
 * Created by Gianni on 3/9/2017.
 */

/**
 * SortString class
 * <p>
 *     Sorts an array of string based on its compareTo, using three different types of sorting mechanisms; Selection
 *     sort, Insertion Sort and Bubble sort.
 * </p>
 */
public class SortString {

    /**
     * Selection Sort
     * <p>
     *     The algorithm finds the minimum value, swaps it with the value in the first position,
     *     and repeats these steps for the remainder of the list. It does no more than n swaps,
     *     and thus is useful where swapping is very expensive.
     * </p>
     * @param a
     * @param k
     */
    public String[] selection(String[] a, String k){
        int minimumIndex = 0;
        int l = 0;
        for(int j=0; j < a.length; j++){
            for (int i=l; i < a.length; i++){
                if(a[minimumIndex].compareTo(a[i]) == 1) minimumIndex = i;
            }
            String minimum = a[minimumIndex];
            a[minimumIndex] = a[j];
            a[j] = minimum;
            l++;
        }
        return a;
    }

    /**
     * Insertion Sort
     * <p>
     *     It works by taking elements from the list one by one and inserting them in their
     *     correct position into a new sorted list.
     * </p>
     * @param a
     * @param k
     */
    public String[] insertion(String[] a, String k){
        for(int i=1; i < a.length; i++){
            if(a[i].compareTo(a[i-1]) == -1){
                for(int j=i-1; j > -1; j--){
                    if(a[i].compareTo(a[j]) == 1){
                        String item = a[i];
                        for(int l=i-1; l < j; l--){
                            a[l+1] = a[l];
                        }
                        a[j+1] = item;
                    }
                }
            }
        }
        return a;
    }
    /**
     * Bubble sort
     * <p>
     *     Bubble sort is a simple sorting algorithm that repeatedly steps through the list to be sorted, compares each
     *     pair of adjacent items and swaps them if they are in the wrong order. The pass through the list is repeated
     *     until no swaps are needed, which indicates that the list is sorted.
     * </p>
     * @param a
     * @param k
     */
    public String[] bubble(String[] a, String k){
        for (int length = a.length; length>1; length--){
            for (int i=0;i<length-1;i++){
                if (a[i].compareTo(a[i+1]) == 1){
                    String l = a[i];
                    a[i]= a[i+1];
                    a[i+1]=l;
                }
            }
        }
        return a;
    }
}
