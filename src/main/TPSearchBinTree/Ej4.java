package main.TPSearchBinTree;

import struct.impl.BinarySearchTree;
import struct.impl.DoubleNode;

/**
 * Created by Gianni on 4/19/2017.
 */
public class Ej4 {
    BinarySearchTree<Account> accounts;

    public Ej4() {
        accounts=new BinarySearchTree<>();
    }

    public void addAccount(Account account){
        accounts.insert(account);
    }

    public void deleteAccounts(BinarySearchTree<Account> accountTree){
        if (!accountTree.isEmpty()){
            accounts.eliminate(accountTree.getMin());
            accountTree.eliminate(accountTree.getMin());
            deleteAccounts(accountTree);
        }
    }

    public void deleteAccounts(int from, int to){ //could probably be improved.
        for (int i=from; i<=to; i++){
            Account account = new Account(i);
            if (accounts.exists(account)) accounts.eliminate(account);
        }
    }
}
