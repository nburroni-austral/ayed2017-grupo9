package main.TPStack;

import struct.impl.DynamicStack;

/**
 * class SimpleCalculator
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/24/2017.
 * <p>
 *     Solves simple mathematical operations.
 * </p>
 */
public class SimpleCalculator {

    private DynamicStack<String> stack;

    public SimpleCalculator() {
        stack=new DynamicStack<>();
    }

    /**
     * Receives a mathematical expression and returns its result.
     * @param expression The expression to be evaluated.
     * @return The result as an Integer.
     */
    public int calculate(String expression){
        try{
            parse(expression);
            solve("^");
            solve("/");
            solve("*");
            solve("-");
            solve("+");

            int a = Integer.parseInt(stack.peek());
            stack.empty();
            return a;
        } catch (NumberFormatException e){
            System.out.println("Invalid Expression");
            System.exit(1);
            return 0;
        }
    }

    /**
     * Separates operators from numbers into different strings, then pushes them into a stack.
     * @param expression The mathematical expression to be evaluated.
     */
    private void parse(String expression){
        String temp = "";
        for (int i=0;i<expression.length();i++){
            Character a = expression.charAt(i);
            if (a=='+' || a=='-' || a=='*' || a=='/' || a=='^'){
                stack.push(temp);
                temp=a.toString();
                stack.push(temp);
                temp = "";
            }else{
                temp = temp + a.toString();
            }
        }
        stack.push(temp);
    }

    /**
     * Solves individual operations for a given operator.
     * @param operator The operator of the operation.
     */
    private void solve(String operator){
        DynamicStack<String> temp = new DynamicStack<>();
        int times = stack.size();
        int result = 0;
        for(int i=0; i<times; i++){
            if (stack.peek().equals(operator)){
                stack.pop();
                int before = Integer.parseInt(temp.peek());
                int after = Integer.parseInt(stack.peek());
                switch (operator){
                    case "*": result = after * before;
                        break;
                    case "/": result = before / after;
                        break;
                    case "+": result = after + before;
                        break;
                    case "-": result = before - after;
                        break;
                    case "^": result = after;
                            for (int j=1; j < before; j++){
                                result = result * after;
                            }
                        break;
                }
                stack.pop();
                temp.pop();
                temp.push(Integer.toString(result));
                times--;
            }else{
                temp.push(stack.peek());
                stack.pop();
            }
        }
        stack = temp;
    }
}
