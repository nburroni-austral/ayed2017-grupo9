package struct.impl.Arbol234;

/**
 * class Node
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/26/2017.
 * <p>
 *     This abstract class defines the behaviour Nodes of a tree234 should have.
 * </p>
 */
public abstract class Node<T extends Comparable<T>> {
    private Node<T> father;
    public int type;
    public int x;
    public int y;

    public abstract Node<T> search(T c);

    public abstract boolean isLeaf();

    public abstract Node<T> insert(T object);

    public abstract void setChild(T o,Node<T> child);

    public abstract void print();

    public Node<T> getFather() {
        return father;
    }

    public abstract Object[] getData();

    public void setFather(Node<T> father) {
        this.father = father;
    }

}
