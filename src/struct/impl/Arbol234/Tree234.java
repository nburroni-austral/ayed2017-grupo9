package struct.impl.Arbol234;
/**
 * class Tree234
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/26/2017.
 */
public class Tree234<T extends Comparable<T>> {

    private Node<T> root;

    public Tree234(T o) {
        Node2<T> node2 = new Node2<>();
        node2.data1 = o;
        root = node2;
        //root.x = 400;
    }

    public Node<T> getRoot() {
        return root;
    }

    public void insert(T o){
        Node<T> node = root.search(o);
        node = node.insert(o);
        Node<T> father = node.getFather();
        if(father!=null){
            while(father.getFather() != null){
                father = father.getFather();
            }
            root = father;
        }
        else{
            root = node;
        }
    }

    public void print() {
        root.print();
    }
}
