package main.TP11;

/**
 * class StudentNotFoundExc
 *Created this class to be able to catch this specific exception.
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  6/19/2017
 */
public class StudentNotFoundExc extends RuntimeException {
}
