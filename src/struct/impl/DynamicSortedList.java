package struct.impl;

import struct.istruct.list.GeneralList;
import struct.istruct.list.SortedList;

import java.io.Serializable;

/**
 * Created by Dwape on 5/10/17.
 */
public class DynamicSortedList<T extends Comparable> implements SortedList<T>, Serializable {

    private Node<T> head, window, sentinel;
    private int size;

    public DynamicSortedList(){
        head = new Node<>();
        sentinel = new Node<>();
        head.next = sentinel;
        window = head;
        size = 0;
    }

    public void insert (T obj){ //check where it leaves window
        Node<T> node = new Node<>(obj);
        window = head;

        while (window.next != sentinel && window.next.obj.compareTo(obj) < 0){
            goNext();
        }
        node.next = window.next;
        window.next = node;
        size++;
    }

    @Override
    public T getActual() {
        return window.obj;
    }
    @Override
    public int getActualPosition() {
        int pos = 0;
        if (!isVoid()) {
            Node<T> aux = head;
            for (; aux.next != window; pos++) aux = aux.next;
        }
        return pos; }
    @Override
    public boolean isVoid() {
        return head.next == sentinel;
    }
    @Override
    public boolean endList() {
        return window.next == sentinel;
    }
    @Override
    public GeneralList<T> clone() {
        return null;
    }
    @Override
    public void goNext() {
        if(window.next == sentinel) throw new IndexOutOfBoundsException("Reached the end of this List");
        window = window.next;
    }
    @Override
    public void goPrev() {
        if(window == head.next) throw new IndexOutOfBoundsException("Reached the beginning of this List");
        goBack(); }
    private void goBack(){
        Node<T> aux = head;
        while(window != aux.next){
            aux = aux.next;
        }
        window = aux;
    }
    @Override
    public void goTo(int index) {
        window = head.next;
        for(int i = 0; i < index; i++){
            window = window.next;
        }
    }
    @Override
    public void remove() {
        if(isVoid()) throw new NullPointerException("This List is empty");
        goBack();
        window.next = window.next.next;
        window = window.next;
        if(window == sentinel) goBack();
        size--; }
    @Override
    public int size() {
        return size;
    }
    private static class Node<E> implements Serializable{
        E obj;
        Node<E> next;
        Node() {
            obj = null;
            next = null; }
        Node(E o) {
            obj = o;
            next = null; }
        Node(E o, Node<E> next) {
            this(o);
            this.next = next;
        }
        boolean hasNoObj() {
            return obj == null;
        }
    }
}
