package main.TPQueue.Ej2;

import struct.impl.DynamicQueue;

/**
 * class Client
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/11/17.
 * <p>
 *     Represents a bank client which makes decisions as to which queue to chose, or whether they
 *     want to stay or not.
 * </p>
 */
public class Client {

    public Client(){

    }

    /**
     * Chooses an index a random index from the provided array.
     * @param queues The array of possible decisions
     * @return The chosen index
     */
    public int chooseQueue(DynamicQueue[] queues){
        return (int) (Math.random() * queues.length);
    }

    /**
     * Determines whether the client will leave the bank upon entering or will stay.
     * @param numberOfPeople The amount of people currently in queues
     * @return True if he leaves, false otherwise
     */
    public boolean leaves(int numberOfPeople){
        if (numberOfPeople < 4) return false;
        if (numberOfPeople < 9){
            int i = (int) (Math.random() * 4);
            return i < 2;
        }
        int i = (int) (Math.random() * 2);
        return i < 1;
    }
}
