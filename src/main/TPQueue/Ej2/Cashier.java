package main.TPQueue.Ej2;

import struct.impl.DynamicQueue;

/**
 * class Cashier
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/11/17.
 * <p>
 *     A cashier from a bank, it has an associated queue and a minimum and maximum time of operation.
 * </p>
 */
public class Cashier {
    private DynamicQueue<Client> queue;

    private int max;
    private int min;
    private int timeToGo;
    private boolean empty;
    private int peopleAttended;

    public Cashier(DynamicQueue queue, int max, int min){
        this.queue = queue;
        this.max = max;
        this.min = min;
        empty = true;
        peopleAttended = 0;
    }

    /**
     * Advances by the minimum unit of time.
     * @param interval The minimum unit of time defined
     */
    public void nextInterval(double interval){
        timeToGo -= interval;
        if (timeToGo <= 0){
            empty = true;
            start();
        }
    }

    /**
     * Returns the cashier's associated queue
     * @return The queue
     */
    public DynamicQueue getQueue(){
        return queue;
    }

    public boolean isEmpty(){
        return empty;
    }

    /**
     * Starts an operation in a cashier, determining the time it will take and occupying it.
     */
    public void start(){
        if (!queue.isEmpty()){
            queue.dequeue();
            timeToGo = getTime();
            empty = false;
            peopleAttended++;
        }
    }

    private int getTime(){
        return  (int) (Math.random() * (max-min) + min);
    }

    public int getPeopleAttended(){
        return peopleAttended;
    }
}
