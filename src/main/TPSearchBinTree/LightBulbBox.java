package main.TPSearchBinTree;

/**
 * class LighBulbBox
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/24/17.
 * <p>
 *     Represents a box with light bulbs.
 * </p>
 */
public class LightBulbBox implements Comparable{

    private String code;
    private int watts;
    private String type;
    private int quantity;

    public LightBulbBox(String code, int watts, String type, int quantity){
        this.code = code;
        this.watts = watts;
        this.type = type;
        this.quantity = quantity;
    }

    public LightBulbBox(String code){
        this.code = code;
    }

    public String getCode(){
        return code;
    }

    public int compareTo(Object lighBulb){
        LightBulbBox bulb = (LightBulbBox) lighBulb;
        return this.code.compareTo(bulb.getCode());
    }
}
