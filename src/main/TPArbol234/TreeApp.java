package main.TPArbol234;

import main.TP10.Scanner;
import struct.impl.Arbol234.Tree234;

/**
 * class TreeApp
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/26/2017.
 */
public class TreeApp implements Runnable{
    View view;

    public TreeApp(View view) {
        this.view = view;
    }

    public void run() {
        while(true){
            try{
                view.update();
                Thread.sleep(100);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int dni = Scanner.getInt("Enter DNI: ");
        Tree234 tree = new Tree234(dni);
        View view = new View(tree);
        TreeApp treeApp = new TreeApp(view);
        Thread thread = new Thread(treeApp);
        thread.start();
        tree.print();
    }
}
