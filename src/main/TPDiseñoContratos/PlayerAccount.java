package main.TPDiseñoContratos;

/**
 * class PlayerAccount
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since 3/11/2017
 * <p>
 *     This class represents a player's account in a videogame.
 * </p>
 */

public class PlayerAccount {

    private String name;
    private String username;
    /*@ invariant username.length() > 3 && username.length() < 20;

     @*/
    private String password;
    /*@ invariant password.length() > 8 && password.length() < 24;

     @*/
    private int[] recoveryCode;
    /*@ invariant \forall int i; recoveryCode[i] != null

     @*/
    public PlayerAccount(String name, String username, String password, int[] recoveryCode) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.recoveryCode = recoveryCode;
    }

    /**
     * changePassword
     * <p>
     *     Changes the old password to a new password.
     * </p>
     * @param newPassword the new password to be set.
     */

   /*@ requires !newPassword.equals(\old(password));
           assignable password;
        @*/
    public void changePassword(/*@ non_null @*/ String newPassword){
        password=newPassword;
    }

    /*@ ensures \result.equals(username);

     @*/
    public /*@ pure @*/ String getUsername() {
        return username;
    }

    /**
     * recoverAccount
     * <p>
     *     Checks if the code entered by the user is the same as the recovery
     *     code for his account, if so, his password is changed to the one he provides.
     * </p>
     * @param newPassword the new password to be set
     * @param recoveryAttempt the code entered by the user that will be compared to the account’s recovery code.
     */

   /*@ requires recoveryAttempt.length == recoveryCode.length
              !newPassword.equals(\old(password));
              assignable password;

    @*/
    public void recoverAccount(int[] recoveryAttempt,String newPassword){
        for(int i=0; i < recoveryCode.length; i++){
            if(recoveryAttempt[i] != recoveryCode[i]) return;
        }
        password=newPassword;
    }

    /**
     * login
     * <p>
     *     Returns true if the user's input is equal to the account's password.
     * </p>
     * @param input the password entered by the user.
     * @return true if the password is correct, false otherwise.
     */
   /*@ ensures \result == input.equals(password);

    @*/
    public boolean login(String input){
        return input.equals(password);
    }
}
