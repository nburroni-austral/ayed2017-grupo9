package main.TPStack;

/**
 * class ParkingLot
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/24/2017.
 * <p>
 *     This class simulates a Car.
 * </p>
 */
public class Car {
    private int licensePlate;//used for comparing cars.

    public Car(int licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getLicensePlate() {
        return licensePlate;
    }
}
