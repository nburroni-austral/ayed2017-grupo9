package main.TPStack;

import struct.impl.DynamicStack;

import java.io.*;
import java.nio.file.Path;

/**
 * class LexicographicAnalyzer
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/24/2017.
 * <p>
 *     Analyses a file to see if it is lexicographically valid.
 * </p>
 */
public class LexicographicAnalyzer {

    private DynamicStack<Character> stack;

    public LexicographicAnalyzer(){
        stack = new DynamicStack<>();
    }

    /**
     * Checks whether a provided file is lexicographically valid.
     * @param file The file to be lexicographically analyzed.
     * @return True if the file is lexicographically valid, false otherwise
     */
    public boolean check(File file){
        stack.empty();
        read(file);
        int parenthesis = 0;
        int braces = 0;
        int brackets = 0;
        while(stack.size() > 0 && parenthesis>=0 && braces>=0 && brackets>=0){
            char symbol = stack.peek();
            switch (symbol){
                case ')': parenthesis++;
                    break;
                case '}': braces++;
                    break;
                case ']': brackets++;
                    break;
                case '(': parenthesis--;
                    break;
                case '{': braces--;
                    break;
                case '[': brackets--;
            }
            stack.pop();
        }
        return parenthesis==0 && braces==0 && brackets==0;
    }

    /**
     * Reads the file, pushing every character into a stack.
     * @param file the file to be read
     */
    private void read(File file){

        Path path = file.toPath();
        String pathString = path.toString();
        String name = file.getName();
        try{
            FileReader reader = new FileReader(pathString);

            int data = reader.read();
            while(data != -1) {
                char dataChar = (char) data;
                data = reader.read();
                stack.push(dataChar);
            }
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + name + "'");
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + name + "'");
        }

    }
}
