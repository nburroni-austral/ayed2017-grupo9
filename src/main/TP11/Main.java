package main.TP11;

import main.TPArbolesBinarios.IndexElement;
import struct.impl.BinarySearchTree;

import java.io.*;

/**
 * class Main
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  6/19/2017
 */
public class Main {
    public static void main(String[] args) {
        mainMenu();
    }

    private static void mainMenu(){
        boolean b1 = true;
        while (b1){
            System.out.println("1. Add new student");
            System.out.println("2. Delete student");
            System.out.println("3. Update student");
            System.out.println("4. Show student info");
            System.out.println("5. Amount of students");
            System.out.println("6. Amount of students whose surname begins with A");
            System.out.println("7. List of students");
            System.out.println("8. List of students whose surname begins with A");
            System.out.println("9. Generate index file");
            System.out.println("10. Print index file");
            System.out.println("11. Exit");
            int option = Scanner.getInt("Choose an option: ");
            switch (option){
                case 1:
                    addStudent();
                    break;
                case 2:
                    deleteStudent();
                    break;
                case 3:
                    updateStudent();
                    break;
                case 4:
                    showStudentInfo();
                    break;
                case 5:
                    printAmountOfStudents();
                    break;
                case 6:
                    printAmountOfAStudents();
                    break;
                case 7:
                    showStudents();
                    break;
                case 8:
                    showAStudents();
                    break;
                case 9:
                    generateIndexFile();
                    break;
                case 10:
                    printTreeFromMinToMax(deserializeTree("src//main//TP11//Tree"));
                    break;
                default:
                    b1=false;
                    break;
            }
        }
    }

    private static void addStudent(){
        char nameInitial;
        char surnameInitial;
        int dni;
        double enrollment;
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            studentFile.goToEnd();
            nameInitial = Scanner.getChar("Name Initial: ");
            surnameInitial = Scanner.getChar("Surname Initial: ");
            dni = Scanner.getInt("DNI: ");
            enrollment = Scanner.getDouble("Enrollment number: ");
            studentFile.write(new Student(nameInitial,surnameInitial,dni,enrollment,true));
            studentFile.close();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static void deleteStudent(){
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            int dni = Scanner.getInt("DNI: ");
            if (studentFile.delete(dni))
                System.out.println("Student deleted");
            else
                System.out.println("Student was not found");
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Allows the user to change a students variables.
     */
    private static void updateStudent(){
        int dni = Scanner.getInt("DNI: ");
        char name = '0';
        char surname = '0';
        int newDni = 0;
        double enrollment = 0;
        StudentFile studentFile = null;
        try {
            studentFile = new StudentFile("src/main/TP11/School");
            studentFile.search(dni);
            studentFile.close();
        }catch (StudentNotFoundExc exc){
            System.out.println("Student not found");
            mainMenu();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean b1 = true;
        while (b1){
            System.out.println("1. Change name and surname");
            System.out.println("2. Change dni");
            System.out.println("3. Change enrollment number");
            System.out.println("4. I don't want to update anything else...");
            int option = Scanner.getInt("Choose an option: ");
            switch (option){
                case 1:
                    name = Scanner.getChar("New name initial: ");
                    surname = Scanner.getChar("New surname initial: ");
                    break;
                case 2:
                    newDni = Scanner.getInt("New DNI: ");
                    break;
                case 3:
                    enrollment = Scanner.getDouble("New enrollment number: ");
                    break;
                default:
                    b1=false;
                    break;
            }
        }
        try {
            studentFile = new StudentFile("src/main/TP11/School");
            studentFile.update(dni,name,surname,newDni,enrollment);
            System.out.println("Student updated");
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static void showStudentInfo(){
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            int dni = Scanner.getInt("DNI: ");
            studentFile.showStudentInfo(dni);
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static void printAmountOfStudents(){
        Student student;
        int counter =0;
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            long amount = studentFile.amountOfReg();
            for (long i = 0; i < amount ; i++){
                student = studentFile.read();
                if (student.isActive()) counter++;
            }
            System.out.println(counter);
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Prints the amount of student whose surname begins with A.
     */
    private static void printAmountOfAStudents(){
        Student student;
        int counter =0;
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            long amount = studentFile.amountOfReg();
            for (long i = 0; i < amount ; i++){
                student = studentFile.read();
                if (student.isActive() && (student.getSurnameInitial() == 'A')) counter++;
            }
            System.out.println(counter);
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static void showStudents(){
        Student student;
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            long amount = studentFile.amountOfReg();
            for (long i = 0; i < amount ; i++){
                student = studentFile.read();
                if(student.isActive()){
                    System.out.print("Name Initials: "+student.getNameInitial()+student.getSurnameInitial()+"\t");
                    System.out.println("DNI: "+student.getDni());
                }
            }
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Prints the students whose surnames begin with A.
     */
    private static void showAStudents(){
        Student student;
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            long amount = studentFile.amountOfReg();
            for (long i = 0; i < amount ; i++){
                student = studentFile.read();
                if(student.isActive() && student.getSurnameInitial()== 'A'){
                    System.out.print("Name Initials: "+student.getNameInitial()+student.getSurnameInitial()+"\t");
                    System.out.println("DNI: "+student.getDni());
                }
            }
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static void generateIndexFile(){
        BinarySearchTree<IndexElement> tree = new BinarySearchTree<>();
        Student student;
        int counter = 0;
        try {
            StudentFile studentFile = new StudentFile("src/main/TP11/School");
            long amount = studentFile.amountOfReg();
            for (long i = 0; i < amount ; i++){
                student = studentFile.read();
                if(student.isActive()){
                    tree.insert(new IndexElement(student.getDni(),counter));
                    counter = counter + studentFile.getRegistrySize();
                }
            }
            studentFile.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        serializeTree(tree);
    }

    /**
     * Reads a file to recover a tree that was saved in it.
     * @param filename The path to the file to be read
     * @return The recovered tree
     */
    private static BinarySearchTree<IndexElement> deserializeTree(String filename) {

        BinarySearchTree<IndexElement> address = null;
        FileInputStream fin = null;
        ObjectInputStream ois = null;

        try {
            fin = new FileInputStream(filename);
            ois = new ObjectInputStream(fin);
            address = (BinarySearchTree) ois.readObject();
            //fin.close();
            ois.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return address;
    }

    /**
     * Writes a tree into a file, this file can be later read to recover the saved tree.
     * @param tree The tree to be saved
     */
    private static void serializeTree(BinarySearchTree tree) {

        FileOutputStream fout = null;
        ObjectOutputStream oos = null;

        try {
            fout = new FileOutputStream("src//main//TP11//Tree");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(tree);
            //fout.close();
            oos.close();
            System.out.println("Done");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Prints the elements contained in a tree from the minimum to the maximum.
     * Used to check if the index file was generated correctly.
     * @param tree the given tree.
     */
    private static void printTreeFromMinToMax(BinarySearchTree<IndexElement> tree){
        System.out.println("Index file elements from min to max: ");
        while (!tree.isEmpty()){
            IndexElement ie = tree.getMin();
            System.out.println(ie.getDni());
            tree.eliminate(ie);
        }
    }
}
