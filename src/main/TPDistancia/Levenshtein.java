package main.TPDistancia;

/**
 * class Levenshtein
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/16/17.
 * <p>
 *     This class calculates the distance between two words using the Levenshtein Method.
 * </p>
 */
public class Levenshtein {

    /**
     * distanceCalc method, calculates distance using the Levenshtein Method.
     *
     * @param string1 one of the words you wish to compare
     * @param string2 the other word you wish to compare
     * @return returns an int value representative of the distance between the words
     */
    public static int distanceCalc(String string1, String string2){ //determines which is the longest and shortest String
        int charDifference = Math.abs(string1.length() - string2.length());
        String shorterString;
        String longerString;
        if (string1.length() > string2.length()) {
            shorterString = string2;
            longerString = string1;
        } else {
            shorterString = string1;
            longerString = string2;
        }
        if(charDifference == 0){ //calls the private method distanceCalc twice in case the strings are of the same length
            int counter1 = distanceCalc(longerString, shorterString, charDifference);
            int counter2 = distanceCalc(shorterString, longerString, charDifference);
            return (counter1+counter2)/2; //returns the average of the two, fixes some cases where the method would work differently depending on the order of the Strings
        }
        return distanceCalc(longerString, shorterString, charDifference);
    }

    private static int distanceCalc(String longerString, String shorterString, int charDifference){
        int counter = longerString.length(); //the counter is set to the longer String's length, this is the upper bound

        for (int i = 0; i < shorterString.length(); i++) {
            for (int j = i; j < i+charDifference+2 && j<longerString.length(); j++) {
                if (shorterString.charAt(i) == longerString.charAt(j)) {
                    counter--; //if we find a useful letter, we decrease counter by one. This letter must be closer to the charDifference otherwise using it will not reduce steps
                    break; //we cannot consider a letter twice, it must exit the inner for when found
                }
            }

        }
        return counter;
    }
}
