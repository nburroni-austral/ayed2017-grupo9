package struct.impl;

import struct.istruct.Stack;
/**
 * class ParkingLot
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/24/2017.
 * <p>
 *     This class is an implementation of Stack using Dynamic Memory.
 * </p>
 */
public class DynamicStack<T> implements Stack<T>{

    private Node<T> n;
    private int size;

    public DynamicStack() {
        size = 0;
    }

    /**
     * pop method
     * <p>
     *     Eliminates the object at the top.
     * </p>
     */
    public void pop() {
        n=n.next;
        size--;
    }

    /**
     * push method
     * <p>
     *     Adds the object to the stack (always at the top of the stack).
     * </p>
     * @param o object to be added, needs to be the same type as the other objects in the stack
     */
    public void push(T o) {
        Node<T> aux = new Node<>(o);
        aux.next=n;
        n=aux;
        size++;
    }

    /**
     * size method
     * <p>
     *     Returns the amount of elements in the stack.
     * </p>
     * @return
     */
    public int size() {
        return size;
    }

    /**
     * peek method
     * @return returns the object at the top, does not eliminate it.
     */
    public T peek() {
        return n.data;
    }

    /**
     * empty method
     * <p>
     *     Empties the stack
     * </p>
     */
    public void empty() {
        n=null;
        size=0;
    }

    /**
     * isEmpty method
     * @return boolean which indicates if the stack is empty or not.
     */
    public boolean isEmpty() {
        return size==0;
    }
}
