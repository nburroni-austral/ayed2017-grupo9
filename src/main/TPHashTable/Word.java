package main.TPHashTable;

import struct.istruct.Hashable;

/**
 * Created by Dwape on 6/19/17.
 */
public class Word implements Hashable, Comparable{

    private String word;

    public Word(String string){
        word = string;
    }

    public int hashCode(int capacity){
        String code = Soundex.soundex(word);
        code = code.substring(1);
        return Integer.parseInt(Integer.toString(Integer.parseInt(code, 7), 10)); //look for better way to do this
    }

    public int compareTo(Object word){
        Word aWord = (Word) word;
        return this.word.compareTo(aWord.getString());
    }

    public String getString(){
        return word;
    }
}
