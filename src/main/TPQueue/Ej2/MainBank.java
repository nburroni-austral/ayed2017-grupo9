package main.TPQueue.Ej2;

import struct.impl.DynamicQueue;

/**
 * class MainBank
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/11/17.
 * <p>
 *     MainBank is used for testing purposes for Bank and all its related classes.
 * </p>
 */
public class MainBank {

    public static void main(String[] args) {

        StrategyA method1 = new StrategyA(1);
        Cashier[] cashiers = new Cashier[3];
        cashiers[0] = new Cashier(new DynamicQueue(), 15, 5);
        cashiers[1] = new Cashier(new DynamicQueue(), 20, 5);
        cashiers[2] = new Cashier(new DynamicQueue(), 25, 5);
        Bank bankOfAmerica = new Bank(method1, cashiers);
        System.out.println(bankOfAmerica.startWorkDay());


        StrategyB method2 = new StrategyB(1);
        Cashier[] cashiers2 = new Cashier[3];
        DynamicQueue queue = new DynamicQueue();
        cashiers2[0] = new Cashier(queue, 15, 5);
        cashiers2[1] = new Cashier(queue, 20, 5);
        cashiers2[2] = new Cashier(queue, 25, 5);
        Bank bank2 = new Bank(method2, cashiers2);
        System.out.println(bank2.startWorkDay());

        System.out.println("");

        StrategyA methodA = new StrategyA(1);
        Cashier[] cashiers3 = new Cashier[3];
        cashiers3[0] = new Cashier(new DynamicQueue(), 20, 15);
        cashiers3[1] = new Cashier(new DynamicQueue(), 35, 20);
        cashiers3[2] = new Cashier(new DynamicQueue(), 50, 40);
        Bank bankOfAmerica3 = new Bank(methodA, cashiers3);
        System.out.println(bankOfAmerica3.startWorkDay());

        StrategyB methodB = new StrategyB(1);
        Cashier[] cashiers4 = new Cashier[3];
        DynamicQueue queue2 = new DynamicQueue();
        cashiers4[0] = new Cashier(queue2, 20, 15);
        cashiers4[1] = new Cashier(queue2, 35, 20);
        cashiers4[2] = new Cashier(queue2, 50, 40);
        Bank bank4 = new Bank(methodB, cashiers4);
        System.out.println(bank4.startWorkDay());

        System.out.println("");

        StrategyA methodA1 = new StrategyA(1);
        Cashier[] cashiers5 = new Cashier[3];
        cashiers5[0] = new Cashier(new DynamicQueue(), 2, 1);
        cashiers5[1] = new Cashier(new DynamicQueue(), 8, 5);
        cashiers5[2] = new Cashier(new DynamicQueue(), 15, 14);
        Bank bankOfAmerica5 = new Bank(methodA1, cashiers5);
        System.out.println(bankOfAmerica5.startWorkDay());


        StrategyB methodB1 = new StrategyB(1);
        Cashier[] cashiers6 = new Cashier[3];
        DynamicQueue queue3 = new DynamicQueue();
        cashiers6[0] = new Cashier(queue3, 2, 1);
        cashiers6[1] = new Cashier(queue3, 8, 5);
        cashiers6[2] = new Cashier(queue3, 15, 14);
        Bank bank6 = new Bank(methodB1, cashiers6);
        System.out.println(bank6.startWorkDay());

    }
}
