package main.TP11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * class StudentFile
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  6/19/2017
 */
public class StudentFile {

    private File f;
    private RandomAccessFile raf;
    private int registrySize;

    public StudentFile(String nombre)throws FileNotFoundException {
        f = new File(nombre);
        raf = new RandomAccessFile(f,"rw");
        registrySize = 17;
    }

    public void write(Student student) throws IOException {
        raf.writeChar(student.getNameInitial());
        raf.writeChar(student.getSurnameInitial());
        raf.writeInt(student.getDni());
        raf.writeDouble(student.getEnrollment());
        raf.writeBoolean(student.isActive());
    }
    public void close() throws IOException {
        raf.close();
    }
    public long length()throws IOException{
        return raf.length();
    }
    public Student read() throws IOException{
        return new Student(raf.readChar(),raf.readChar(),raf.readInt(), raf.readDouble(), raf.readBoolean());
    }
    public long amountOfReg() throws IOException {
        return raf.length()/registrySize;
    }
    public void goToStart() throws IOException{
        raf.seek(0);
    }
    public void goToEnd() throws IOException{
        raf.seek(raf.length());
    }
    public void goTo(long reg) throws IOException{
        raf.seek((reg-1)*registrySize);
    }

    public int getRegistrySize() {
        return registrySize;
    }

    public Student search(int dni) throws IOException{
        long amount = amountOfReg();
        goToStart();
        Student student;
        for (int i =0 ; i < amount;i++){
            student = read();
            if(student.isActive() && (student.getDni()==dni))
                return student;
        }
        throw new StudentNotFoundExc();
    }

    public boolean delete(int dni) throws IOException{
        try {
            Student student = search(dni);
            raf.seek(raf.getFilePointer()-registrySize);
            student.setActive(false);
            write(student);
            return true;
        }catch (StudentNotFoundExc exc){
            return false;
        }
    }

    public boolean update(int dni, char name, char surname, int newDni, double enrollment) throws IOException{
        try {
            Student student = search(dni);
            raf.seek(raf.getFilePointer()-registrySize);
            if (name!='0'){
                student.setNameInitial(name);
                student.setSurnameInitial(surname);
            }
            if (newDni!=0) student.setDni(newDni);
            if (enrollment!=0) student.setEnrollment(enrollment);
            write(student);
            return true;
        }catch (StudentNotFoundExc exc){
            return false;
        }
    }

    public void showStudentInfo(int dni) throws IOException {
        try {
            Student student = search(dni);
            System.out.println("Name Initials: "+student.getNameInitial()+student.getSurnameInitial());
            System.out.println("DNI: "+student.getDni());
            System.out.println("Enrollment number: "+student.getEnrollment());
        }catch (StudentNotFoundExc exc){
            System.out.println("Student was not found");
        }
    }

}
