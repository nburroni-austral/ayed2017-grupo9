package main.TPHashTable;

import struct.impl.DynamicList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * class Controller
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  7/4/2017
 */
public class Controller {
    private HashTableWindow mainWindow;
    private CorrectorWindow correctorWindow;
    private Corrector corrector;
    private DynamicList<String> incorrectWords;

    public Controller(){
        corrector = new Corrector(5);
        mainWindow = new HashTableWindow(correct);
        correctorWindow = new CorrectorWindow(next); //check which parameters are needed.
    }

    ActionListener correct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            //String newText = corrector.spellCheck(mainWindow.getText()); //how it should work.
            //mainWindow.updateText(newText);
            correctorWindow.suggestionsPanel.removeAll();
            incorrectWords = new DynamicList<>();
            incorrectWords = corrector.getIncorrectWords(mainWindow.getText());
            incorrectWords.goTo(0);
            correctorWindow.showSuggestions(incorrectWords.getActual(),corrector.suggestWord(new Word(incorrectWords.getActual())));
            incorrectWords.remove();
            //mainWindow.setVisible(false);
            correctorWindow.setVisible(true);
        }
    };

    ActionListener next = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            correctorWindow.suggestionsPanel.removeAll();
            if (!incorrectWords.isVoid()){
                incorrectWords.goTo(0);
                correctorWindow.showSuggestions(incorrectWords.getActual(),corrector.suggestWord(new Word(incorrectWords.getActual())));
                incorrectWords.remove();
            }else {
                correctorWindow.setVisible(false);
            }
        }
    };

    /*
    ActionListener choose = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            mainWindow.suggestionsPanel.removeAll();
        }
    };
    */
}
