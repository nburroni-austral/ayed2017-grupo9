package main.TPSearchBinTree;

import struct.impl.BinarySearchTree;

import java.util.ArrayList;

/**
 * class Stock
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/24/17.
 * <p>
 *     Stores light bulb boxes and allows certain manipulation methods.
 * </p>
 */
public class Stock {

    BinarySearchTree<LightBulbBox> storage;

    public Stock(){
        storage = new BinarySearchTree<>();
    }

    /**
     * Searches for a light bulb with a given code.
     * @param code The given code
     * @return true if it is found, false otherwise
     */
    public LightBulbBox search(String code){
        LightBulbBox box = new LightBulbBox(code);
        return storage.search(box);
    }

    /**
     * Stores a light bulb box in the storage.
     * @param box The box to be stored
     */
    public void store(LightBulbBox box){
        storage.insert(box);
    }

    /**
     * Removes a light bulb from storage from a given code.
     * @param code The code of the box to be removed
     */
    public void remove(String code){
        LightBulbBox box = new LightBulbBox(code);
        storage.eliminate(box);
    }

    /**
     * Removes a light bulb from storage.
     * @param box The light bulb to be removed
     */
    public void remove(LightBulbBox box){
        storage.eliminate(box);
    }

    /**
     * Returns all the elements in order as an ArrayList.
     * @param tree The storage
     * @return All the elements in order
     */
    public static ArrayList report(BinarySearchTree tree){
        ArrayList elements = new ArrayList();
        return inorder(tree,elements);
    }

    private static ArrayList inorder(BinarySearchTree tree, ArrayList elements){
        if (tree.isEmpty()) return elements;
        inorder(tree.getLeft(),elements);
        elements.add(tree.getRoot());
        inorder(tree.getRight(),elements);
        return elements;
    }
}
