package main.TPEquipos;

import struct.impl.DynamicStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * class FootballTable
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/19/2017.
 * <p>
 *     This class determines the outcome of each of the games played, knowing only the number of points that each team
 *     has achieved(0 for defeat, 1 for tie, and 3 for victory), and matches disputed to date.
 *     (works similarly to Sudoku)
 * </p>
 */
public class FootballTable {
    private ArrayList<DynamicStack<Character>> output;
    private int amountOfTeams, matchesPlayed;
    private HashMap<String, Integer> scores;
    private ArrayList<Match> matches;
    private int index;

    public FootballTable(int amountOfTeams, int matchesPlayed, HashMap<String, Integer> scores, ArrayList<Match> matches) {
        output = new ArrayList<>();
        this.amountOfTeams = amountOfTeams;
        this.matchesPlayed = matchesPlayed;
        this.scores = scores;
        this.matches = matches;
        index = 0;
        fillOutput();
    }


    private void fillOutput(){
        for (int i=0; i< matchesPlayed; i++){
            DynamicStack<Character> stack = new DynamicStack<>();
            stack.push('X');
            stack.push('2');
            stack.push('1');
            output.add(stack);
        }
    }

    /**
     * If the home team won,"1"; If the away team wins "2"; If there is a tie, write "X".
     * @return returns an array with the results of each match,
     */
    public ArrayList<DynamicStack<Character>> solve(){
        iterateSolve();
        return output;
    }

    private void iterateSolve(){
        while (index < output.size()){
            solveFor();
        }
        if (index == output.size() && !checkFinalScores()){
            goBack();
            iterateSolve();
        }
    }

    private void solveFor(){

        switch (output.get(index).peek()){
            case '1':
                String homeTeam = matches.get(index).getHomeTeam();
                int score = scores.get(homeTeam);
                if (score>2){
                    scores.replace(homeTeam,score-3);
                    index++;
                }else {
                    tryDiffResult();
                }
                break;
            case '2':
                String awayTeam = matches.get(index).getAwayTeam();
                int score2 = scores.get(awayTeam);
                if (score2>2){
                    scores.replace(awayTeam,score2-3);
                    index++;
                }else {
                    tryDiffResult();
                }
                break;
            case 'X':
                String homeTeam2 = matches.get(index).getHomeTeam();
                int scoreHome = scores.get(homeTeam2);
                String awayTeam2 = matches.get(index).getAwayTeam();
                int scoreAway = scores.get(awayTeam2);
                if (scoreHome>0 && scoreAway>0){
                    scores.replace(homeTeam2,scoreHome-1);
                    scores.replace(awayTeam2,scoreAway-1);
                    index++;
                }else {
                    tryDiffResult();
                }
                break;

        }
    }

    private void tryDiffResult(){
        if (output.get(index).size()>=2){
            output.get(index).pop();
        }else {
            goBack();
        }
    }

    private void goBack(){
        if (index<1)return;

        switch (output.get(index-1).peek()){
            case '1':
                String homeTeam = matches.get(index-1).getHomeTeam();
                int score = scores.get(homeTeam);
                scores.replace(homeTeam,score+3);
                break;
            case '2':
                String awayTeam = matches.get(index-1).getAwayTeam();
                int score2 = scores.get(awayTeam);
                scores.replace(awayTeam,score2+3);
                break;
            case 'X':
                String homeTeam2 = matches.get(index-1).getHomeTeam();
                int scoreHome = scores.get(homeTeam2);
                String awayTeam2 = matches.get(index-1).getAwayTeam();
                int scoreAway = scores.get(awayTeam2);
                scores.replace(homeTeam2,scoreHome+1);
                scores.replace(awayTeam2,scoreAway+1);
                break;
        }
        if (index < matchesPlayed) refresh(output.get(index));
        index--;
        tryDiffResult();
    }

    private void refresh(DynamicStack<Character> stack){
        stack.empty();
        stack.push('X');
        stack.push('2');
        stack.push('1');
    }

    private boolean checkFinalScores(){
        Collection<Integer> finalScores = scores.values();
        Integer[] scoreBoard = new Integer[scores.size()];
        finalScores.toArray(scoreBoard);
        for (int i=0; i < scoreBoard.length; i++){
            if (scoreBoard[i] != 0){
                return false;
            }
        }
        return true;
    }
}
