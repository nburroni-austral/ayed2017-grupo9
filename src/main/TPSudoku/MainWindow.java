package main.TPSudoku;

import javax.swing.*;
        import javax.swing.border.Border;
        import javax.swing.text.MaskFormatter;
        import java.awt.*;
        import java.awt.event.ActionListener;

/**
 * class MainWindow
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/1/17.
 * <p>
 *     Manages the graphical user interface (GUI) of Sudoku.
 * </p>
 */
public class MainWindow extends JFrame{

    private JFormattedTextField[][] board;
    private boolean[][] fixed = new boolean[9][9];
    private JLabel label;

    /**
     * Creates a MainWindow.
     * @param solve action listener used for solve button
     * @param reset action listener used for reset button
     */
    public MainWindow(ActionListener solve, ActionListener reset){
        setTitle("Sudoku");
        setPanels(solve, reset);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    /**
     * Makes the window visible.
     */
    public void showSelf(){
        setVisible(true);
    }

    /**
     * Gets the values entered by the user.
     * @return returns a two dimensional int array containing the values entered by the user.
     */
    public Integer[][] getUserInput(){
        Integer[][] inputValues = new Integer[9][9];
        for(int i=0; i < 9; i++){
            for(int j=0; j < 9; j++){
                String text = board[j][i].getText();
                try {
                    Integer value = Integer.parseInt(text);
                    inputValues[j][i] = value;
                    fixed[j][i] = true;
                }catch (NumberFormatException e){

                }
            }
        }
        return inputValues;
    }

    /**
     * Fills the board (JFormattedTextField[][]) with the solution of the sudoku.
     * @param solution multidimensional array of ints which contains the solution of the sudoku.
     */
    public void displaySolution(Integer[][] solution){
        for(int i=0; i < 9; i++){
            for(int j=0; j < 9; j++){
                String number = solution[i][j].toString();
                if(number.equals("0")){
                    board[i][j].setText("");
                } else if(fixed[i][j]){
                    board[i][j].setText(number);
                }
                else{
                    board[i][j].setForeground(Color.blue);
                    board[i][j].setText(number);
                }
            }
        }
    }

    /**
     * Manages how the GUI looks.
     * @param solve action listener used for solve button
     * @param reset action listener used for reset button
     */
    private void setPanels(ActionListener solve, ActionListener reset){
        JPanel mainPanel = new JPanel(new BorderLayout());
        JButton solveButton = new JButton("Solve");
        solveButton.addActionListener(solve);

        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(reset);

        board = new JFormattedTextField[9][9];

        JPanel bigGrid = new JPanel(new GridLayout(3, 3));
        Border border = BorderFactory.createLineBorder(Color.black, 2);
        bigGrid.setBorder(border);
        board = new JFormattedTextField[9][9];

        int a = 0;
        int b = 0;
        for(int i=0; i<9; i++){
            JPanel smallGrid = new JPanel(new GridLayout(3, 3));
            smallGrid.setBorder(border);
            for(int m=a; m<a+3; m++){
                for(int n=b; n<b+3; n++){
                    board[m][n] = createTextField();
                    smallGrid.add(board[m][n]);
                }
            }
            bigGrid.add(smallGrid);
            if (b>5){
                b = 0;
                a = a+3;
            } else{
                b = b+3;
            }
        }
        label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setPreferredSize(new Dimension(200, 10));

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(resetButton);
        buttonPanel.add(label);
        buttonPanel.add(solveButton);
        mainPanel.add(buttonPanel, BorderLayout.PAGE_END);
        mainPanel.add(bigGrid, BorderLayout.CENTER);

        add(mainPanel);
    }

    /**
     * resets the board so it can be used again.
     */
    public void reset(){
        for(int i=0; i < 9; i++){
            for(int j=0; j < 9; j++){
                board[i][j].setForeground(Color.black);
                board[i][j].setText("");
            }
        }
        for(int i=0; i < 9; i++){
            for(int j=0; j < 9; j++){
                fixed[i][j] = false;
            }
        }
        label.setText("");
    }

    /**
     * Changes the label to display a given message
     * @param a message to be displayed
     */
    public void updateLabel(String a){
        label.setText(a);
    }

    private JFormattedTextField createTextField(){
        Border border = BorderFactory.createLineBorder(Color.black, 1);
        JFormattedTextField textField = new JFormattedTextField(createFormatter("#"));
        textField.setPreferredSize(new Dimension(40,40));
        textField.setHorizontalAlignment(JTextField.CENTER);
        Font currentFont = textField.getFont();
        Font font = currentFont.deriveFont((float)25);
        textField.setFont(font);
        textField.setBorder(border);
        return textField;
    }

    private MaskFormatter createFormatter(String s) {
        MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(s);
        } catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
            System.exit(-1);
        }
        return formatter;
    }
}
