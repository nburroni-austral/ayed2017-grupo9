package main.TP11;

/**
 * class Student
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  6/19/2017
 */
public class Student {
    private char nameInitial;
    private char surnameInitial;
    private  int dni;
    private double enrollment;
    private boolean active;

    public Student(char nameInitial, char surnameInitial, int dni, double enrollment, boolean active) {
        this.nameInitial = nameInitial;
        this.surnameInitial = surnameInitial;
        this.dni = dni;
        this.enrollment = enrollment;
        this.active = active;
    }

    public char getNameInitial() {
        return nameInitial;
    }

    public char getSurnameInitial() {
        return surnameInitial;
    }

    public int getDni() {
        return dni;
    }

    public double getEnrollment() {
        return enrollment;
    }

    public boolean isActive() {
        return active;
    }

    public void setNameInitial(char nameInitial) {
        this.nameInitial = nameInitial;
    }

    public void setSurnameInitial(char surnameInitial) {
        this.surnameInitial = surnameInitial;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public void setEnrollment(double enrollment) {
        this.enrollment = enrollment;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
