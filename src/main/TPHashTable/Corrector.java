package main.TPHashTable;

import struct.impl.DynamicList;
import struct.impl.HashTable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * class Corrector
 * Corrects a String by checking which words are in the dictionary, those that aren't are replaced by a suggestion chosen
 * by the user.
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  6/20/2017.
 */
public class Corrector {

    private HashTable<Word> dictionary;
    private int amountOfSuggestions; //maximum number of suggestions.
    private int maxTolerance; //the maximum distance that is tolerated for suggested words.

    public Corrector(int amountOfSuggestions){
        dictionary = new HashTable<>(342);
        this.amountOfSuggestions = amountOfSuggestions;
        maxTolerance = 6; //should be variable.
        loadDictionary();
    }

    /**
     * Loads the dictionary in words_alpha.txt, adding all the words to the HashTable that will be used.
     */
    private void loadDictionary(){
        try{
            FileReader fileReader = new FileReader("src/main/TPHashTable/words_alpha.txt");
            BufferedReader br = new BufferedReader(fileReader);
            String str = br.readLine();
            while (str!=null){
                dictionary.insert(new Word(str));
                str=br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *Suggests replacements for words that are not included in the dictionary.
     * @param string The string to be corrected.
     * @return The string corrected, with the words replaced by the suggested ones that the user chose.
     */
    public String spellCheck(String string){
        String[] strings = string.split(" |\\.|\n");
        for (int i=0; i<strings.length; i++){
            Word word = new Word(strings[i]);
            Word lowerWord = new Word(word.getString().toLowerCase());
            if (!dictionary.search(lowerWord)){
                strings[i] = chooseWord(suggestWord(word)).getString();
            }
        }
        String corrected = "";
        for (int i=0; i<strings.length; i++){
            corrected += strings[i];
            corrected += " ";
        }
        return corrected;
    }

    public DynamicList<String> getIncorrectWords(String string){
        String[] strings = string.split(" ");// |\.|
        DynamicList<String> incorrectWords = new DynamicList<>();
        for (int i=0; i<strings.length; i++){
            Word word = new Word(strings[i]);
            Word lowerWord = new Word(word.getString().toLowerCase());
            if (!dictionary.search(lowerWord)){
                incorrectWords.insertNext(word.getString());
            }
        }
        return incorrectWords;
    }

    /**
     * Suggests possible replacements for a word provided.
     * @param word The word to be replaced.
     * @return A DynamicList with all the possible replacements. The list will have a size amountOfSuggestions+1 or lower,
     * depending on maxTolerance. The first word is the original word entered by the user.
     */
    public DynamicList<Word> suggestWord(Word word){
        DynamicList<Word> suggestions = dictionary.t[word.hashCode(0)];
        DynamicList<Word> filteredSuggestions = new DynamicList<>();
        filteredSuggestions.insertNext(word);
        for (int j=1; j<=maxTolerance; j++){
            for (int i=1; i<suggestions.size(); i++){
                if (filteredSuggestions.size()>amountOfSuggestions) break; //inefficient?
                suggestions.goTo(i);
                int distance = Levenshtein.levenshteinDistance(suggestions.getActual().getString(), word.getString());
                if (distance < j){
                    filteredSuggestions.insertNext(suggestions.getActual());
                    suggestions.remove();
                }
            }
        }
        return filteredSuggestions;
    }

    /**
     * Allows a user to choose from a variety of suggested words through console.
     * @param suggestions The possible words to choose from.
     * @return The word chosen.
     */
    public Word chooseWord(DynamicList<Word> suggestions){
        if (suggestions.size() == 1){
            suggestions.goTo(0);
            return suggestions.getActual();
        }
        for(int i=1; i<suggestions.size(); i++){
            suggestions.goTo(i);
            System.out.println((i) + ". " + suggestions.getActual().getString());
        }
        System.out.println("0. SKIP");
        int chosen = Scanner.getInt("Choose an option: ");
        if (!(chosen>0 && chosen<suggestions.size())) chosen = 0;
        suggestions.goTo(chosen);
        return suggestions.getActual();
    }
}
