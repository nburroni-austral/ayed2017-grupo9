package struct.impl;

import java.io.Serializable;

/**
 * Created by Gianni on 4/6/2017.
 */
public class DoubleNode<T> implements Serializable {
    T data;
    DoubleNode <T> right;
    DoubleNode <T> left;

    public DoubleNode(T o){
        data = o;
    }

    public DoubleNode(T o, DoubleNode<T> left, DoubleNode<T> right){
        data = o;
        this.right = right;
        this.left = left;
    }
}
