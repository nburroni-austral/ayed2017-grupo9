package main.TPMetrovias;

import struct.impl.DynamicList;

/**
 * class TrainMain
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/25/2017.
 * <p>
 *     Tests the functioning of Station and its related classes.
 * </p>
 */
public class TrainMain {

    public static void main(String[] args) {

        Station station = new Station(10);
        DynamicList<DynamicList<String>> result = station.startWorkDay();

    }
}
