package main.TP10;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Dwape on 5/21/17.
 */
public class Ej2 {

    public static void main(String[] args) {
        String fileName = Scanner.getString("Enter the file's name: ");
        char character = Scanner.getChar("Enter the character you want to look for: ");
        int counter = occurences(fileName, character);
        System.out.println("That character appears " + counter + " times in that file.");
    }

    public static int occurences(String fileName, char character){
        FileReader fileReader;
        int counter = 0;
        try{
            fileReader = new FileReader("src/main/TP10/" + fileName); //file path could differ
            int a = 0;
            while(a!=-1){
                a = fileReader.read();
                if (a == character) counter++;
            }

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        return counter;
    }
}
