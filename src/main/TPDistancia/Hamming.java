package main.TPDistancia;

/**
 * class Hamming
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/16/17.
 * <p>
 *     This class calculates the distance between two words using the Hamming method
 * </p>
 */
public class Hamming {
    /**
     * distanceCalc method
     * @param word1 one of the words you wish to compare
     * @param word2 the other word you wish to compare
     * @return returns an int value representative of the distance between the words
     */
    /*@ requires word1.length()==word2.length();
     @*/
    public static int distanceCalc(String word1, String word2){
        int counter=0;
        for (int i=0; i<word1.length(); i++){
            if (word1.charAt(i)!=word2.charAt(i)){
                counter++;
            }
        }
        return counter;
    }
}
