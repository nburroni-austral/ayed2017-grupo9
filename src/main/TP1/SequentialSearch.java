package main.TP1;

/**
 * Created by Dwape on 3/7/17.
 */
public class SequentialSearch {

    public SequentialSearch(){
    }

    public boolean sequentialSearch(int[] a, int k){
        for(int i=0; i<a.length; i++){
            if (a[i] == k) return true;
        }
        return false;
    }

    public int sequentialSearchIndex(int[] a, int k){
        for(int i=0; i<a.length; i++){
            if (a[i] == k) return i;
        }
        throw new RuntimeException(); //element not found.
    }
}
