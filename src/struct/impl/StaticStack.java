package struct.impl;

import struct.istruct.Stack;
/**
 * class ParkingLot
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/24/2017.
 * <p>
 *     This class is an implementation of Stack using Static Memory.
 * </p>
 */
public class StaticStack<T> implements Stack<T>{

    private int top;
    private int capacity;
    private Object[] data;

    public StaticStack(int x){
        top = -1;
        capacity = x;
        data = new Object[capacity];
    }

    /**
     * push method
     * <p>
     *     Adds the object to the stack (always at the top of the stack).
     * </p>
     * @param o object to be added, needs to be the same type as the other objects in the stack
     */
    public void push(T o){
        if (top+1 == data.length) grow();
        data[top+1] = o;
        top++;
    }

    /**
     * pop method
     * <p>
     *     Eliminates the object at the top.
     * </p>
     */
    public void pop(){
        top--;
    }

    /**
     * peek method
     * @return returns the object at the top, does not eliminate it.
     */
    @SuppressWarnings("unchecked")
    public T peek(){
        return (T) data[top];
    }

    /**
     * isEmpty method
     * @return boolean which indicates if the stack is empty or not.
     */
    public boolean isEmpty(){
        return top == -1;
    }

    /**
     * size method
     * <p>
     *     Returns the amount of elements in the stack.
     * </p>
     * @return
     */
    public int size(){
        return top+1;
    }

    /**
     * empty method
     * <p>
     *     Empties the stack
     * </p>
     */
    public void empty(){
        top = -1;
    }

    private void grow(){
        Object[] data2 = new Object[capacity*2];
        for(int i=0; i<capacity; i++){
            data2[i] = data[i];
        }
        data = data2;
    }
}
