package struct.impl.RBTree;

/**
 * Created by Dwape on 5/26/17.
 */
public class RBNode<T extends Comparable> {

    public T data;
    public RBNode<T> father;
    public RBNode<T> right;
    public RBNode<T> left;
    public boolean isRed;
    public int x;
    public int y;

    public RBNode(T o){
        data = o;
        isRed = true;
    }

    public RBNode(T o, RBNode<T> father){
        data = o;
        this.father = father;
        isRed = true;
    }

    public RBNode(T o, RBNode<T> left, RBNode<T> right, RBNode<T> father){
        data = o;
        this.father = father;
        this.right = right;
        this.left = left;
        isRed = true;
    }
}
