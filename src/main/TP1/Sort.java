package main.TP1;

/**
 * Created by Dwape on 3/8/17.
 */


import java.util.ArrayList;
import java.util.Comparator;

/**
 * Sort class
 * <p>
 *     Sorts an array of any type of data based on its compareTo, using three different types of sorting mechanisms; Selection
 *     sort, Insertion Sort and Bubble sort.
 * </p>
 */
public class Sort <T extends Comparable>{

    public Sort(){
    }

    /**
     * Selection Sort
     * <p>
     *     The algorithm finds the minimum value, swaps it with the value in the first position,
     *     and repeats these steps for the remainder of the list. It does no more than n swaps,
     *     and thus is useful where swapping is very expensive.
     * </p>
     * @param a
     */
    public T[] selection(T[] a){
        int minimumIndex = 0;
        for(int j=0; j < a.length-1; j++){
            for (int i=j; i < a.length; i++){
                if(a[minimumIndex].compareTo(a[i]) == 1) minimumIndex = i;
            }
            T minimum = a[minimumIndex];
            a[minimumIndex] = a[j];
            a[j] = minimum;
            minimumIndex=j+1;
        }
        return a;
    }
    /** Selection Sort Time Testing
     * N=10 -> 13ms
     * N=100 -> 17ms
     * N=1000 -> 46ms
     * N=10000 -> 606ms
     */

    private T[] recursiveSelection(T[] a, int minimumIndex){
        if(minimumIndex == a.length-1) return a;
        int j = minimumIndex;
        for (int i=j; i < a.length; i++){
            if(a[minimumIndex].compareTo(a[i]) == 1) minimumIndex = i;
        }
        T minimum = a[minimumIndex];
        a[minimumIndex] = a[j];
        a[j] = minimum;
        minimumIndex = j;
        recursiveSelection(a, minimumIndex+1);
        return a;
    }

    /**
     * Does the same as selection but recursively.
     * @param a
     */
    public T[] recursiveSelection(T[] a){
        return recursiveSelection(a, 0);
    }

    /**
     * Insertion Sort
     * <p>
     *     It works by taking elements from the list one by one and inserting them in their
     *     correct position into a new sorted list.
     * </p>
     * @param a
     */
    public T[] insertion(T[] a) {
        for (int i = 1; i < a.length; i++) {
            T l = a[i];
            int j;
            for (j = i-1; j>=0 && l.compareTo(a[j])==-1; j--){
                a[j+1]=a[j];
            }
            a[j+1]=l;
        }
        return a;
    }
    /** Insertion Sort Time Testing
     * N=10 -> 13ms
     * N=100 -> 18ms
     * N=1000 -> 49ms
     * N=10000 -> 600ms
     */

    /**
     * Bubble sort
     * <p>
     *     Bubble sort is a simple sorting algorithm that repeatedly steps through the list to be sorted, compares each
     *     pair of adjacent items and swaps them if they are in the wrong order. The pass through the list is repeated
     *     until no swaps are needed, which indicates that the list is sorted.
     * </p>
     * @param a
     */
    public T[] bubble(T[] a){
        for (int length = a.length; length>1; length--){
            for (int i=0;i<length-1;i++){
                if (a[i].compareTo(a[i+1]) == 1){
                    T l = a[i];
                    a[i]= a[i+1];
                    a[i+1]=l;
                }
            }
        }
        return a;
    }
    /** Bubble Sort Time Testing
     * N=10 -> 13ms
     * N=100 -> 18ms
     * N=1000 -> 84ms
     * N=10000 -> 1s 819ms
     */

    /**
     * Merge
     * <p>
     *     Combines two sorted arrays, creating a sorted ArrayList with their items.
     * </p>
     * @param a
     * @param b
     */

    public Comparable<T>[] merge(T[] a, T[] b){//both a and b are sorted
        Comparable<T>[] c = new Comparable[a.length+b.length];
        int aCount = 0;
        int bCount = 0;
        int cCount = 0;
        while(cCount<a.length+b.length){
            if (aCount==a.length){
                c[cCount]=b[bCount];
                cCount++;
                bCount++;
            }else if (bCount==b.length){
                c[cCount]=a[aCount];
                cCount++;
                aCount++;
            }else if (a[aCount].compareTo(b[bCount])==-1){
                c[cCount]=a[aCount];
                cCount++;
                aCount++;
            }else {
                c[cCount]=b[bCount];
                cCount++;
                bCount++;
            }
        }
        return c;
    }

}
