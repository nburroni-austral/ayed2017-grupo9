package main.TPSudoku;

import struct.impl.DynamicStack;

/**
 * class Sudoku
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/4/17.
 * <p>
 *     Solves a game of Sudoku, using backtracking. A board can be provided, and the numbers in it are
 *     fixed.
 * </p>
 */
public class Sudoku {
    private DynamicStack<Integer>[][] board;
    private Boolean[][] fixed;
    private int x;
    private int y;
    private boolean isGoingBack;

    public Sudoku() {
        board = new DynamicStack[9][9];
        fixed = new Boolean[9][9];
        x = 0;
        y = 0;
        fillWithZeros();
        fillWithFalse();
        refresh(0,0);
    }

    /**
     * Solves the Sudoku for the current x, y position, evaluating the current value of the top
     * element in board for that position.
     */
    public void solve(){

        if(isGoingBack) goBack();
        else if(fixed[x][y]){
            isGoingBack = false;
            advance();
        } else if(testSameRow() && testSameColumn() && testSameSquare()){
            advance();
        } else{
            nextValue();
        }
    }

    /**
     * Fixes several values what will work as the starting condition for the resolution of the Sudoku.
     * @param values A two-dimensional array with the values to be set
     */
    public void setValues(Integer[][] values){
        for(int i=0; i < 9; i++){
            for(int j=0; j < 9; j++){
                if (values[i][j] != null){
                    fixValue(i, j, values[i][j]);
                }
            }
        }
    }

    /**
     * Fixes an individual value for a provided position.
     * @param x The x value of the position
     * @param y The y value of the position
     * @param value The value that board[x][y] will be fixed to
     */
    public void fixValue(int x, int y, int value){
        DynamicStack<Integer> fixedValue = new DynamicStack<>();
        fixedValue.push(value);
        board[x][y]=fixedValue;
        fixed[x][y]=true;
    }

    /**
     * Returns the current state of board. After the sudoku is solved, if a solution was found, it will return its
     * solution. Intermediate calls with return intermediate steps of the solution.
     * @return The current state of the board as a two-dimensional array of Integers
     */
    public Integer[][] getSolution(){
        Integer[][] solution = new Integer[9][9];
        for(int i=0; i < 9; i++){
            for(int j=0; j < 9; j++){
                solution[i][j] = board[i][j].peek();
            }
        }
        return solution;
    }

    private void advance(){
        if (x==8&&y==8){
            x++;
            return;
        }
        if (y < 8) {
            y++;
            refresh(x,y);
        } else {
            y=0;
            x++;
            refresh(x,y);
        }
    }

    private void goBack(){
        if(x==0 && y==0){
            y--;
            isGoingBack = false;
            return;
        }
        setToZero(x, y);
        if (y>0){
            y--;
            if (board[x][y].size()<2){
                setToZero(x, y);
                isGoingBack = true;
            } else {
                board[x][y].pop();
                isGoingBack = false;
            }
        }else{
            y=8;
            x--;
            if (board[x][y].size()<2){
                setToZero(x, y);
                isGoingBack = true;
            } else {
                board[x][y].pop();
                isGoingBack = false;
            }
        }
    }

    private void nextValue(){
        if (board[x][y].size()>1){
            board[x][y].pop();
        }else{
            goBack();
        }
    }

    private void refresh(int x, int y){
        if (fixed[x][y]) return;
        DynamicStack<Integer> refreshedStack = new DynamicStack<>();
        for (int i=1; i<10;i++){
            refreshedStack.push(i);
        }
        board[x][y]=refreshedStack;
    }


    private boolean testSameRow(){
        for (int i = 0; i<9; i++){
            try{
                if ((board[x][i].peek().equals(board[x][y].peek())) && (y!=i)) return false;
            } catch (NullPointerException e){
            }
        }
        return true;
    }

    private boolean testSameColumn(){
        for (int i = 0; i<9; i++){
            try{
                if ((board[i][y].peek().equals(board[x][y].peek())) && (x!=i)) return false;
            } catch (NullPointerException e){
            }
        }
        return true;
    }

    private boolean testSameSquare(){
        int xSquare = xPositionSquare(x);
        int ySquare = yPositionSquare(y);
        for (int i = xSquare; i<xSquare+3; i++){
            for (int j = ySquare; j<ySquare+3; j++){
                try{
                    if ((board[i][j].peek().equals(board[x][y].peek())) && (x!=i) && (y!=j)) return false;
                } catch (NullPointerException e){
                }
            }
        }
        return true;
    }

    private int xPositionSquare(int x){
        if (x < 3) return 0;
        else if (x < 6) return 3;
        else return 6;
    }

    private int yPositionSquare(int y){
        if (y < 3) return 0;
        else if (y < 6) return 3;
        else return 6;
    }

    private void fillWithZeros(){
        for (int i=0; i<9 ; i++){
            for (int j=0; j<9 ; j++){
                DynamicStack<Integer> stack = new DynamicStack<>();
                stack.push(0);
                board[i][j] = stack;
            }
        }
    }

    private void setToZero(int x, int y){
        if (fixed[x][y]) return;
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(0);
        board[x][y] = stack;
    }

    private void fillWithFalse(){
        for (int i=0; i<9 ; i++){
            for (int j=0; j<9 ; j++){
                fixed[i][j] = false;
            }
        }
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }
}
