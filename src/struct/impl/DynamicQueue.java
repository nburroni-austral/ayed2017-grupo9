package struct.impl;

import struct.istruct.Queue;

/**
 * class DynamicQueue
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/9/2017.
 * <p>
 *     This class is an implementation of Queue using dynamic memory.
 * </p>
 */
public class DynamicQueue<T> implements Queue<T> {
    private int size;
    private Node<T> front;
    private Node<T> back;

    public DynamicQueue() {
        size=0;
    }

    public void enqueue(T t) {
        Node<T> aux = new Node<>(t);
        if(isEmpty()){
            back = aux;
            front = aux;
        }
        back.next=aux;
        back=aux;
        size++;
    }

    public T dequeue() {
        if (isEmpty()) throw new RuntimeException("Queue is empty");
        T data = front.data;
        front = front.next;
        size--;
        return data;
    }

    public void empty() {
        front=null;
        back=null;
        size=0;
    }

    public boolean isEmpty() {
        return size==0;
    }

    public int size() {
        return size;
    }

    public int length() {
        return size;
    }
}
