package main.TPMetrovias;

import struct.impl.DynamicList;

/**
 * class Station
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/25/2017.
 * <p>
 *    A train station where tickets are sold.
 * </p>
 */
public class Station {

    private DynamicList<Window> windows;
    private int intervalTime;
    private int openingTime;
    private int closingTime;
    private int currentTime;
    private int passengersInBatch;

    public Station(int numberOfWindows){
        windows = new DynamicList<>();
        for (int i=0; i<numberOfWindows; i++){
            windows.insertNext(new Window());
        }
        windows.goTo(0);
        intervalTime = 10;
        openingTime = 21600;
        closingTime = 79200;
        currentTime = openingTime;
        passengersInBatch = 5;
    }

    /**
     * Starts a work day.
     * @return The final results of the simulation, in a DynamicList
     */
    public DynamicList startWorkDay(){
        while (currentTime < (closingTime - 30)){
            nextInterval();
        }
        finish();
        return getFinalResults();
    }

    /**
     * Advances to the next interval of time.
     */
    private void nextInterval(){
        addPassengers();
        for (int i=0; i<windows.size(); i++){
            windows.goTo(i);
            windows.getActual().nextInterval();
        }
        currentTime += intervalTime;
    }

    /**
     * Adds passenger in a batch of a set size.
     */
    private void addPassengers(){
        for (int i=0; i<passengersInBatch; i++){
            Passenger passenger = new Passenger();
            int chosenWindow = passenger.choseWindow(windows.size());
            windows.goTo(chosenWindow);
            windows.getActual().addPassenger(passenger);
        }
    }

    /**
     * Finishes the working day, attending all waiting passengers.
     */
    private void finish(){

        for (int i=0; i<windows.size(); i++){
            windows.goTo(i);
            windows.getActual().finish();

        }

    }

    /**
     * Prints the final results of the simulation.
     * @return The final results in a DynamicList
     */
    private DynamicList getFinalResults(){
        DynamicList<DynamicList<String>> finalResults = new DynamicList<>();

        for (int i=0; i<windows.size(); i++) {
            windows.goTo(i);
            DynamicList<String> list = new DynamicList<>();
            String waiting = "Waiting Time: " + windows.getActual().getAverageWaitingCycles() * intervalTime;
            String balance = "Balance: " + windows.getActual().getBalance();
            String leisure = "Leisure Time: " + windows.getActual().getLeisureCycles() * intervalTime;

            System.out.println(waiting);
            System.out.println(balance);
            System.out.println(leisure);

            list.insertNext(waiting);
            list.insertNext(balance);
            list.insertNext(leisure);
            finalResults.insertNext(list);
        }
        return finalResults;
    }
}
