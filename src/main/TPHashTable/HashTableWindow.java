package main.TPHashTable;

import struct.impl.DynamicList;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

/**
 * class HashTableWindow
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  7/4/2017
 */
public class HashTableWindow extends JFrame{
    JTextArea field;
    JPanel mainPanel;
    //JPanel suggestionsPanel;
    JMenuBar menuBar;
    JMenu fileMenu;
    JMenu editMenu;
    JMenuItem save;
    JMenuItem spellCheck;
    String text;

    public HashTableWindow(ActionListener correct){
        System.setProperty("apple.laf.useScreenMenuBar", "true"); //MacOS (used to make it nicer)
        setTitle("Grammar Checker");
        setMenus(correct);
        editLayout();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setResizable(true);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void editLayout(){
        mainPanel = new JPanel(new BorderLayout());
        field = new JTextArea();
        field.setLineWrap(true);
        field.setFont(field.getFont().deriveFont(20f));
        mainPanel.add(field,BorderLayout.CENTER);
        add(mainPanel);
    }

    private void setMenus(ActionListener correct){
        menuBar = new JMenuBar();

        editMenu = new JMenu("Edit");
        fileMenu = new JMenu("File");
        save = new JMenuItem("Save");
        spellCheck = new JMenuItem("Spell Check");
        spellCheck.addActionListener(correct);
        editMenu.add(spellCheck);
        fileMenu.add(save);

        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        setJMenuBar(menuBar);
    }

    public String getText(){
        text = field.getText();
        return text;
    }

    /*
    public void updateText(String newText){ //check if it works
        text = newText;
        field.removeAll();
        field.insert(newText, 0);
    }
    */
}
