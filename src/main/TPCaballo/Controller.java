package main.TPCaballo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * class Controller
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/1/17.
 * <p>
 *     Manages "communication" between Sudoku and MainWindow.
 * </p>
 */
public class Controller {

    private MainWindow menuWindow;
    private HorseMovement chess;
    private boolean isStarted;

    /**
     * Creates a Controller that will create a MainWindow, providing the necessary ActionListeners.
     */
    public Controller(){
        menuWindow = new MainWindow(next, start);
        menuWindow.displayBoard(new Position(0, 0));
        isStarted=false;
    }
    /**
     * ActionListeners with the behaviour that each button will follow.
     */
    ActionListener next = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (isStarted){
                menuWindow.deletePos(chess.getHorse());
                chess.next();
                menuWindow.displayBoard(chess.getHorse());
                menuWindow.displayStacks(chess.getStacks());
                menuWindow.updateLabel("Press next to continue.");
            }else{
                menuWindow.updateLabel("Please choose a number of jumps and press start first.");
            }
        }
    };

    ActionListener start = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!isStarted){
                try{
                    int n = menuWindow.getNumberOfJumps();
                    chess = new HorseMovement(n);
                    isStarted=true;
                    menuWindow.updateLabel("Program started! Press next to continue.");
                }catch (NumberFormatException exc){
                    menuWindow.updateLabel("Enter valid number of jumps please.");
                }
            }
        }
    };
}
