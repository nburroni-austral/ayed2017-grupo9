package main.RBTree;
import struct.impl.RBTree.RBNode;
import struct.impl.RBTree.RBTree;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * Created by Dwape on 5/30/17.
 */
public class RBTreeDrawer extends JFrame {

    public static void main(String[] args) {
        RBTreeDrawer drawer = new RBTreeDrawer();
    }

    ArrayList<Shape> shapesBLACK = new ArrayList<>();
    ArrayList<Shape> shapesRED = new ArrayList<>();
    ArrayList<Line2D> lines = new ArrayList<>();
    ArrayList<StringDraw> strings = new ArrayList<>();
    double ellipseSizeX = 90;
    double ellipseSizeY = 50;
    int frameSizeX = 960;
    int frameSizeY = 540;
    public void drawTree(RBTree<Integer> tree){
        preorder(tree);
        add("Center", new MyCanvas());
        setSize(frameSizeX, frameSizeY);
        setTitle("Red Black Tree");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setResizable(false); //could be better if the frame size is fixed.
        setVisible(true);
    }

    public void preorder(RBTree<Integer> tree){
        int height = height(tree.getRoot());
        preorder(tree.getRoot(), 0, height);
    }

    private void preorder(RBNode<Integer> node, int difference, int height){ //first time it will be null
        if (node == null) return;
        if (node.father == null){
            node.x = (int)(Math.pow(2, height)*(ellipseSizeX+difference))+(int)ellipseSizeX; //make it relative to the height
            node.y = 20;
        } else {
            node.x = node.father.x + difference*height; //check if null
            node.y = node.father.y + 50; //check which amount works.
            double radius = ellipseSizeX/2;
            lines.add(new Line2D.Double(node.father.x+radius, node.father.y+radius, node.x+radius, node.y+radius)); //ellipse size
        }
        if (node.isRed){
            shapesRED.add(new Ellipse2D.Double(node.x, node.y, ellipseSizeX, ellipseSizeY));
        } else {
            shapesBLACK.add(new Ellipse2D.Double(node.x, node.y, ellipseSizeX, ellipseSizeY));
        }
        strings.add(new StringDraw(Integer.toString(node.data), node.x+ellipseSizeX/2-(ellipseSizeX/3), node.y+ellipseSizeY/2+(ellipseSizeY/10)));
        preorder(node.left, -60, height-1);
        preorder(node.right, 60, height-1);
    }

    private int height(RBNode<Integer> node){
        if(node == null) return 0;
        if(node.left == null && node.right == null) return 1;
        return 1+Math.max(height(node.left), height(node.right));
    }

    class MyCanvas extends Canvas {
        public void paint(Graphics graphics) {
            Graphics2D black = (Graphics2D) graphics;
            black.setPaint(Color.BLACK);
            for (int i=0; i< lines.size(); ++i){
                if (lines.get(i) != null)
                    black.draw(lines.get(i));
            }
            for (int i = 0; i < shapesBLACK.size(); ++i) {
                if (shapesBLACK.get(i) != null)
                    black.fill(shapesBLACK.get(i));
                    black.draw(shapesBLACK.get(i));
            }

            Graphics2D red = (Graphics2D) graphics;
            red.setPaint(Color.RED);
            for (int i = 0; i < shapesRED.size(); ++i) {
                if (shapesRED.get(i) != null)
                    red.fill(shapesRED.get(i));
                    red.draw(shapesRED.get(i));
            }

            Graphics2D white = (Graphics2D) graphics;
            white.setPaint(Color.WHITE);
            for (int i=0; i< strings.size(); ++i){
                if (strings.get(i) != null)
                    white.drawString(strings.get(i).string, (int)strings.get(i).x, (int)strings.get(i).y);
            }
        }
    }
}
