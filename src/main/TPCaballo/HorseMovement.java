package main.TPCaballo;

import struct.impl.DynamicStack;

/**
 * class HorseMovement
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/4/17.
 * <p>
 *     HorseMovement simulates the movement of a chess horse in a chess board. The user provides the desired
 *     amount of jumps, and the horse will move to every possible position, starting from A1.
 * </p>
 */
public class HorseMovement {

    private Position[][] board;
    private DynamicStack<Position>[] stacks;
    private Position horse;
    private int jumps;
    private boolean isGoingBack;
    private boolean isJumping;

    public HorseMovement(int n){
        stacks = new DynamicStack[n+1];
        DynamicStack<Position> init = new DynamicStack();
        board = new Position[8][8];
        horse = new Position(0,0);
        board[horse.getX()][horse.getY()] = horse;
        init.push(horse);
        stacks[0]=init;
        jumps=0;
        isGoingBack=false;
        isJumping=false;
    }

    /**
     * Determines the next move that the horse must make. All execution paths move the horse exactly once.
     */
    public void next(){

        if (isGoingBack){
            goBack();
        } else if (isJumping){
            jumps++;
            updateHorse(stacks[jumps].peek());
            isJumping=false;
        }else if (jumps==stacks.length-1){
            if (stacks[jumps].size()>1){
                stacks[jumps].pop();
                jumps--;
                updateHorse(stacks[jumps].peek());
                isJumping=true;
            }else {
                goBack();
            }
        }else{
            jumps++;
            stacks[jumps]=possibleMoves(horse);
            updateHorse(stacks[jumps].peek());
        }
    }

    /**
     * Used by next for execution paths in which the horse is required to move to a previous position.
     */
    private void goBack(){
        if (jumps==0)return;
        stacks[jumps].pop();
        jumps--;
        if (stacks[jumps].size()>1){
            stacks[jumps].pop();
            updateHorse(stacks[jumps].peek());
            isGoingBack=false;
        }else{
            updateHorse(stacks[jumps].peek());
            isGoingBack=true;
        }
    }

    /**
     * Changes the horse's position to a specified one.
     * @param position The specified position
     */
    private void updateHorse(Position position){
        board[horse.getX()][horse.getY()]=null;
        horse = position;
        board[horse.getX()][horse.getY()]=horse;

    }

    /**
     * Determines which moves can be made from a specified position.
     * @param position The starting position
     * @return A stack with the possible moves
     */
    private DynamicStack<Position> possibleMoves(Position position){
        DynamicStack<Position> possibilities = new DynamicStack<>();
        int x = position.getX();
        int y = position.getY();

        if (checkBounds(x+2,y+1)){
            Position position1 = new Position(x+2,y+1);
            possibilities.push(position1);
        }
        if (checkBounds(x+2,y-1)){
            Position position1 = new Position(x+2,y-1);
            possibilities.push(position1);
        }
        if (checkBounds(x-2,y+1)){
            Position position1 = new Position(x-2,y+1);
            possibilities.push(position1);
        }
        if (checkBounds(x-2,y-1)){
            Position position1 = new Position(x-2,y-1);
            possibilities.push(position1);
        }

        if (checkBounds(x+1,y+2)){
            Position position1 = new Position(x+1,y+2);
            possibilities.push(position1);
        }
        if (checkBounds(x-1,y+2)){
            Position position1 = new Position(x-1,y+2);
            possibilities.push(position1);
        }
        if (checkBounds(x+1,y-2)){
            Position position1 = new Position(x+1,y-2);
            possibilities.push(position1);
        }
        if (checkBounds(x-1,y-2)){
            Position position1 = new Position(x-1,y-2);
            possibilities.push(position1);
        }
        return possibilities;
    }

    private boolean checkBounds(int x, int y){
        return !(x>7 || x<0 || y>7 || y<0);
    }

    /**
     * Returns the horse's position.
     * @return The horses position.
     */
    public Position getHorse() {
        return horse;
    }

    /**
     * Returns an array with stacks containing all the movements that the horse must still make.
     * @return An array of stacks with the positions
     */
    public DynamicStack<Position>[] getStacks() {
        return stacks;
    }
}
