package main.TPSearchBinTree;

import struct.impl.BinarySearchTree;

/**
 * Created by Gianni on 4/19/2017.
 */
public class Ej5 {
    BinarySearchTree<Integer> tree;

    public Ej5() {
        tree=new BinarySearchTree<>();
    }

    public void insert(Integer integer){
        tree.insert(integer);
    }

    public int range(int keyFrom, int keyTo){
        int counter=0;
        for (int i = keyFrom;i<=keyTo;i++){
            if (tree.exists(i)) counter++;
        }
        return counter;
    }

    /*
    public int range(int keyFrom, int keyTo){
        Element from = new Element(keyFrom,null);
        Element to = new Element(keyTo,null);
        int counter=0;
        for (int i=keyFrom; i<=keyTo;i++){
            Element elementKey = new Element(keyFrom,null);
            tree.search(elementKey);
        }
        return counter;
    }
    */
}
