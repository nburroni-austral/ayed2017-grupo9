package main.TPStack;

import struct.impl.DynamicStack;

import java.io.*;
/**
 * Created by Dwape on 3/24/17.
 */
public class Main {

    public static void main(String[] args) {

        File source = new File("src//main//TPStack//sourcecode");

        LexicographicAnalyzer analyzer = new LexicographicAnalyzer();

        System.out.println(analyzer.check(source));

        SimpleCalculator calc = new SimpleCalculator();

        int a = calc.calculate("2+2");
        System.out.println(a);
    }
}
