package main.TPArbolesBinarios;

import struct.impl.DoubleNode;
import struct.impl.DynamicBinaryTree;
import struct.impl.StaticQueue;

import java.util.ArrayList;

/**
 * class TreeTraversal
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/8/2017.
 */
public class TreeTraversal {

    public static ArrayList preorder(DynamicBinaryTree tree){
        ArrayList elements = new ArrayList();
        return preorder(tree, elements);
    }

    private static ArrayList preorder(DynamicBinaryTree tree, ArrayList elements){
        if (tree.isEmpty()) return elements;
        elements.add(tree.getRoot());
        preorder(tree.getLeft(),elements);
        preorder(tree.getRight(),elements);
        return elements;
    }

    public static ArrayList inorder(DynamicBinaryTree tree){
        ArrayList elements = new ArrayList();
        return inorder(tree,elements);
    }

    private static ArrayList inorder(DynamicBinaryTree tree, ArrayList elements){
        if (tree.isEmpty()) return elements;
        inorder(tree.getLeft(),elements);
        elements.add(tree.getRoot());
        inorder(tree.getRight(),elements);
        return elements;
    }

    public static ArrayList postorder(DynamicBinaryTree tree){
        ArrayList elements = new ArrayList();
        return postorder(tree,elements);
    }

    private static ArrayList postorder(DynamicBinaryTree tree, ArrayList elements){
        if (tree.isEmpty()) return elements;
        postorder(tree.getLeft(),elements);
        postorder(tree.getRight(),elements);
        elements.add(tree.getRoot());
        return elements;
    }

    public static ArrayList inlevels(DynamicBinaryTree tree){
        ArrayList<DynamicBinaryTree> trees = new ArrayList<>();
        if (!tree.isEmpty()) trees.add(tree);
        for (int k=0; k < trees.size(); k++){
            addChildren(trees, trees.get(k));
        }
        ArrayList elements = new ArrayList();
        for (int i=0; i < trees.size(); i++){
            elements.add(trees.get(i).getRoot());
        }
        return elements;
    }

    private static void addChildren(ArrayList trees, DynamicBinaryTree tree){
        if (!tree.getLeft().isEmpty()) trees.add(tree.getLeft());
        if (!tree.getRight().isEmpty()) trees.add(tree.getRight());
    }

    public static void print(ArrayList elements){
        for(int i=0; i<elements.size(); i++){
            System.out.print(elements.get(i));
        }
        System.out.println("");
    }


}
