package main.TPListasOrdenadas;

import java.io.Serializable;

/**
 * class Bus
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/11/2017.
 * <p>
 *     This class simulates a bus.
 * </p>
 */
public class Bus implements Comparable<Bus>, Serializable{

    private int lineNumber;
    private int internalNumber;
    private int numberOfSeats;
    private boolean disabledFriendly;

    public Bus(int lineNumber, int internalNumber, int numberOfSeats, boolean disabledFriendly){
        this.lineNumber = lineNumber;
        this.internalNumber = internalNumber;
        this.numberOfSeats = numberOfSeats;
        this.disabledFriendly = disabledFriendly;
    }

    public int compareTo(Bus bus){
        if (lineNumber > bus.getLineNumber()) return 1;
        if (lineNumber < bus.getLineNumber()) return -1;
        if (internalNumber > bus.getInternalNumber()) return 1;
        if (internalNumber < bus.getInternalNumber()) return -1;
        return 0;
    }

    public int getLineNumber(){
        return lineNumber;
    }

    public int getInternalNumber(){
        return internalNumber;
    }

    public int getNumberOfSeats(){
        return numberOfSeats;
    }

    public boolean isDisabledFriendly(){
        return disabledFriendly;
    }
}
