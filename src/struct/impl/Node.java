package struct.impl;

import java.io.Serializable;

/**
 * class Node
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/24/2017.
 * <p>
 *     This class is used to achieve Dinamic Memory in java.
 * </p>
 */
public class Node<T>{
    public T data;
    public Node<T> next;

    public Node(T data) {
        this.data = data;
    }
}
