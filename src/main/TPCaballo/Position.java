package main.TPCaballo;

/**
 * class Position
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/4/17.
 * <p>
 *     Represents the position of a chess piece in a chess board.
 * </p>
 */
public class Position {
    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Returns the position in the format used in chess, i.e. numbers for rows and letters for columns.
     * @return The position in "chess" format
     */
    public String getDisplayPosition(){
        Character c = (char) (x+65);
        int b = y+1;
        return c+""+b;
    }
}
