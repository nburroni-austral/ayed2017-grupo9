package struct.impl;

import java.io.Serializable;

/**
 * Created by Gianni on 4/18/2017.
 */
public class BinarySearchTree<T extends Comparable> implements Serializable {

    private DoubleNode<T> root;

    public BinarySearchTree() {
        root=null;
    }

    public boolean isEmpty() {
        return root==null;
    }


    public BinarySearchTree<T> getLeft() {
        BinarySearchTree<T> tree = new BinarySearchTree<>();
        tree.root = root.left;
        return tree;
    }


    public BinarySearchTree<T> getRight() {
        BinarySearchTree<T> tree = new BinarySearchTree<T>();
        tree.root = root.right;
        return tree;
    }

    public T getRoot() {
        return root.data;
    }

    public boolean exists(Comparable x){
        return exists(root,x);
    }

    private boolean exists(DoubleNode<T> t, Comparable x){
        if (t==null){
            return false;
        }
        if (x.compareTo(t.data)==0){
            return true;
        }else if (x.compareTo(t.data)<0){
            return exists(t.left,x);
        }else {
            return exists(t.right,x);
        }
    }

    public T getMin(){
        return getMin(root).data;
    }

    private DoubleNode<T> getMin(DoubleNode<T> t){
        if (t.left==null)return t;
        else return getMin(t.left);
    }

    public T getMax(){
        return getMax(root).data;
    }

    private DoubleNode<T> getMax(DoubleNode<T> t){
        if (t.right==null)return t;
        else return getMax(t.right);
    }

    public T search(Comparable x){
        try{
            return search(root,x).data;
        }catch (Exception e){
            throw new RuntimeException("Element not found");
        }

    }

    private DoubleNode<T> search(DoubleNode<T> t, Comparable x){
        if (x.compareTo( t.data)== 0)
            return t;
        else if (x.compareTo( t.data)< 0)
            return search(t.left, x);
        else
            return search(t.right, x);
    }

    public void insert(T x){
        root = insert(root,x);
    }

    private DoubleNode<T> insert(DoubleNode<T> t, T x){
        if (t==null){
            t = new DoubleNode<T>(x);
        }else if (x.compareTo(t.data)<0){
            t.left = insert(t.left,x);
        }else if(x.compareTo(t.data)==0){
            throw new RuntimeException("Element is already in tree");
        }else{
            t.right = insert(t.right,x);
        }
        return t;
    }

    public void eliminate(Comparable x){
        try{
            root = eliminate(root,x);
        }catch (Exception e){
            throw new RuntimeException("Element not found");
        }
    }

    private DoubleNode<T> eliminate(DoubleNode<T> t, Comparable x){
        if (x.compareTo(t.data) < 0) t.left = eliminate(t.left, x);
        else if (x.compareTo(t.data) > 0) t.right = eliminate(t.right, x);
        else{
            if (t.left != null && t.right != null ) {
                t.data = getMin(t.right).data;
                t.right = eliminateMin(t.right);
            }
            else if (t.left != null)
                t = t.left;
            else
                t =t.right;
        }
        return t;
    }

    private DoubleNode<T> eliminateMin(DoubleNode<T> t){
        if (t.left != null)
            t.left = eliminateMin(t.left);
        else
            t = t.right;
        return t;
    }
}
