package main.TP10;

import java.io.*;

/**
 * class Ej4
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/21/2017.
 * <p>
 *     This class solves the problems posed by exercise 4 of TP10, the methods are self explanatory
 * </p>
 */
public class Ej4 {
    public static void main(String[] args) {
        String fileName = Scanner.getString("Enter file name: ");
        separateFile(fileName);//"src/main/TP10/Example"
    }

    private static void separateFile(String fileName){
        try {
            FileReader fileReader = new FileReader(fileName);
            FileWriter fileWriter1 = new FileWriter("src/main/TP10/LessThan30");
            FileWriter fileWriter2 = new FileWriter("src/main/TP10/MoreThan30");
            int a = fileReader.read();
            while (a!=-1){
                String country = "";
                while (a!=44){
                    country = country + (char)a;
                    a=fileReader.read();
                }
                a = fileReader.read();
                String population = "";
                while (a!=44){
                    population = population + (char)a;
                    a=fileReader.read();
                }
                a = fileReader.read();
                String pbi = "";
                while (a!=44){
                    pbi = pbi + (char)a;
                    a=fileReader.read();
                }
                a=fileReader.read();
                a=fileReader.read();
                //a=fileReader.read();
                if (Integer.parseInt(population)<30){
                    fileWriter1.write(country+",");
                    fileWriter1.write(population+",");
                    fileWriter1.write(pbi+",\n");
                }else {
                    fileWriter2.write(country+",");
                    fileWriter2.write(population+",");
                    fileWriter2.write(pbi+",\n");
                }
            }
            fileReader.close();
            fileWriter1.close();
            fileWriter2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
