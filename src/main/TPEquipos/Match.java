package main.TPEquipos;

/**
 * class Match
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/19/2017.
 * <p>
 *     Represents a football match, with the two teams that played it.
 * </p>
 */
public class Match {
    private String homeTeam;
    private String awayTeam;

    public Match(String homeTeam, String awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }
}
