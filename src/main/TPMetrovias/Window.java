package main.TPMetrovias;

import struct.impl.DynamicQueue;

/**
 * class Window
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/25/2017.
 * <p>
 *     A window where train tickets are sold.
 * </p>
 */
public class Window {

    private DynamicQueue<Passenger> queue;
    private int totalWaitingCycles;
    private double balance;
    private int leisureCycles;
    private int numberOfClientsAttended;
    private double price;

    public Window(){
        queue = new DynamicQueue<>();
        totalWaitingCycles = 0;
        balance = 0;
        leisureCycles = 0;
        numberOfClientsAttended = 0;
        price = 0.7;
    }

    /**
     * Advances one cycle.
     */
    public void nextInterval(){
        if (queue.isEmpty()) leisureCycles ++;
        else {
            if (Math.random()<=0.3) {
                Passenger attended = queue.dequeue();
                totalWaitingCycles += attended.getWaitingCycles();
                balance += price;
                numberOfClientsAttended++;
            }
            int queueSize = queue.size();
            for (int i=0; i < queueSize; i++){
                Passenger waiting = queue.dequeue();
                waiting.incrementWait();
                queue.enqueue(waiting);
            }
        }
    }

    /**
     * Adds a passenger to its queue
     * @param passenger The passenger to be added
     */
    public void addPassenger(Passenger passenger){
        queue.enqueue(passenger);
    }

    /**
     * Finished attending passengers for the day, any waiting passenger will be attended.
     */
    public void finish(){
        while (!queue.isEmpty()){
            Passenger attended = queue.dequeue();
            totalWaitingCycles += attended.getWaitingCycles();
            balance += price;
            numberOfClientsAttended++;
        }
    }

    /**
     * Returns the average amount of waiting cycles.
     * @return The average amount of waiting cycles
     */
    public double getAverageWaitingCycles(){
        double result = totalWaitingCycles/numberOfClientsAttended;
        return result;
    }

    public int getLeisureCycles(){
        return leisureCycles;
    }

    public double getBalance(){
        return balance;
    }
}
