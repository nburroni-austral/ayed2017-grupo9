package main.TPArbolesBinarios;

import struct.impl.DynamicBinaryTree;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * class WriteObject
 * <p>
 *     Saves a DynamicBinaryTree into a file.
 * </p>
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/8/2017.
 */
public class WriteObject {
    /**
     * Writes a tree into a file, this file can be later read to recover the saved tree.
     * @param tree The tree to be saved
     */
    public static void serializeTree(DynamicBinaryTree tree) {

        FileOutputStream fout = null;
        ObjectOutputStream oos = null;

        try {

            fout = new FileOutputStream("src//main//TPArbolesBinarios//Tree");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(tree);

            System.out.println("Done");

        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {

            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
