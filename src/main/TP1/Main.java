package main.TP1;

import java.util.ArrayList;

/**
 * Created by Gianni on 3/9/2017.
 */
public class Main {
    public static void main(String[] args) {

        Integer[] randomArray = RandomArrayGenerator.generateRandomArray(10); //tongueTwister

        Sort sort = new Sort();

        for (int i = 0; i < randomArray.length;i++){
            System.out.println(randomArray[i]);
        }

        sort.recursiveSelection(randomArray);

        System.out.println("");
        System.out.println("");
        System.out.println("");

        for (int i = 0; i < randomArray.length;i++){
            System.out.println(randomArray[i]);
        }


        Integer[] randomArray1 = RandomArrayGenerator.generateRandomArray(5);
        Integer[] randomArray2 = RandomArrayGenerator.generateRandomArray(5);
        sort.recursiveSelection(randomArray1);
        sort.recursiveSelection(randomArray2);

        for (int i = 0; i < randomArray1.length;i++){
            System.out.println(randomArray1[i]);
        }
        System.out.println("");
        System.out.println("");
        for (int i = 0; i < randomArray2.length;i++){
            System.out.println(randomArray2[i]);
        }

        System.out.println("");
        System.out.println("");
        System.out.println("");

        Comparable[] mergeArray = sort.merge(randomArray1,randomArray2);

        for (int i = 0; i < mergeArray.length;i++){
            System.out.println(mergeArray[i]);
        }

    }

}
