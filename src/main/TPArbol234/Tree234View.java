package main.TPArbol234;

/**
 * Created by Gianni on 5/30/2017.
 */
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import javax.swing.JFrame;

public class Tree234View extends JFrame{
    private ArrayList<Shape> shapes = new ArrayList<>();

    public static void main(String args[]) {
        Tree234View app = new Tree234View();
    }
    /*
    public static void main(String[] args) {
        RBTreeDrawer drawer = new RBTreeDrawer();
    }

    ArrayList<Shape> shapes;
    public void drawTree(RBTree tree){
        shapes = preorder(tree);
        add("Center", new MyCanvas());
        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static ArrayList<Shape> preorder(RBTree tree){
        ArrayList<Shape> shapes = new ArrayList<>();
        return preorder(tree.getRoot(), shapes, 0);
    }

    private static ArrayList<Shape> preorder(RBNode node, ArrayList<Shape> shapes, int difference){ //first time it will be null
        if (node == null) return shapes;
        if (node.father == null){
            node.x = 100;
            node.y = 20;
        } else {
            node.x = node.father.x + difference; //check if null
            node.y = node.father.y + 50; //check which amount works.
        }
        shapes.add(new Ellipse2D.Double(node.x, node.y, 50, 50));
        preorder(node.left,shapes, -60);
        preorder(node.right,shapes, 60);
        return shapes;
    }

    class MyCanvas extends Canvas {
        public void paint(Graphics graphics) {
            Graphics2D g = (Graphics2D) graphics;
            for (int i = 0; i < shapes.size(); ++i) {
                if (shapes.get(i) != null)
                    g.draw(shapes.get(i));
            }
        }
    }
    */
    /*
    public static void main(String[] args) {

        RBTree<Integer> tree = new RBTree<>();

        int number = Scanner.getInt("Enter an int: ");
        while (number != 0){
            tree.insert(number);
            System.out.println("Int added.");
            number = Scanner.getInt("Enter an int: ");
        }
        RBTreeDrawer draw = new RBTreeDrawer();
        draw.drawTree(tree);
    }
     */
}
