package main.TPMetrovias;

/**
 * class Passenger
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/25/2017.
 * <p>
 *     Represents a train passenger.
 * </p>
 */
public class Passenger {

    int waitingCycles;

    public Passenger(){
        waitingCycles = 0;
    }

    public int getWaitingCycles(){
        return waitingCycles;
    }

    /**
     * Adds 1 to the passenger's total waiting cycles
     */
    public void incrementWait(){
        waitingCycles++;
    }

    /**
     * Chooses a random window from the provided indexes
     * @param numberOfWindows The amount of windows to choose from
     * @return The chosen number of window
     */
    public int choseWindow(int numberOfWindows){
        return (int) (Math.random()*numberOfWindows);
    }
}
