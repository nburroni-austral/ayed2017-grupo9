package main.TPQueue.Ej1;

import struct.impl.DynamicQueue;
import struct.impl.PriorityQueue;
import struct.impl.StaticQueue;

/**
 * class Main
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/5/17.
 */
public class Main {

    public static void main(String[] args) {
        testDynamicQueue();
        System.out.println(" ");
        testStaticQueue();
        System.out.println(" ");
        testPriorityQueue();
        System.out.println(" ");
        System.out.println(Palindrome.checkIfPalindrome("SOME"));

    }

    private static void testPriorityQueue(){
        PriorityQueue<Double> queue = new PriorityQueue<>(3);

        queue.enqueue(1.2,2);
        queue.enqueue(4.1,1);
        queue.enqueue(6.1,1);
        queue.enqueue(9.3,3);
        queue.enqueue(6.2,2);
        queue.enqueue(7.2,2);
        queue.enqueue(3.3,3);
        queue.enqueue(5.3,3);

        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
    }

    private static void testDynamicQueue(){
        DynamicQueue<Integer> dynamicQueue = new DynamicQueue<>();
        dynamicQueue.enqueue(1);
        dynamicQueue.enqueue(2);
        System.out.println(dynamicQueue.dequeue());
        dynamicQueue.enqueue(3);
        dynamicQueue.enqueue(4);
        System.out.println(dynamicQueue.dequeue());
        System.out.println(dynamicQueue.dequeue());
        dynamicQueue.enqueue(45);
        System.out.println(dynamicQueue.dequeue());
        dynamicQueue.enqueue(5);
        System.out.println(dynamicQueue.dequeue());
    }

    private static void testStaticQueue(){
        StaticQueue<Integer> queue = new StaticQueue<>(6);


        queue.inqueue(1);
        queue.inqueue(2);
        queue.inqueue(3);
        queue.inqueue(4);
        queue.inqueue(5);
        queue.inqueue(6);

        queue.dequeue();
        queue.dequeue();

        queue.inqueue(7);
        queue.inqueue(8);
        queue.inqueue(9);


        queue.inqueue(1);
        queue.inqueue(2);
        queue.inqueue(3);
        queue.inqueue(4);
        queue.inqueue(5);
        queue.inqueue(6);

        queue.inqueue(7);
    }
}
