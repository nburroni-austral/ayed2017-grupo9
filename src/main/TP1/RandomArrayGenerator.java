package main.TP1;

import java.util.Random;

/**
 * Created by Dwape on 3/9/17.
 */

/**
 * RandomArrayGenerator
 * <p>
 *     Generates arrays of the desired length with random integers.
 * </p>
 */
public class RandomArrayGenerator {

    /**
     * Creates and array of length n with random integers.
     * @param n
     * @return randomArray An array of length n with random integers.
     */
    public static Integer[] generateRandomArray(int n){
        Integer[] randomArray = new Integer[n];
        for(int i=0; i<randomArray.length; i++){
            Random rand = new Random();
            Integer value = rand.nextInt(1000);
            randomArray[i] = value;
        }
        return randomArray;
    }
}
