package struct.impl;

import struct.istruct.BinaryTree;

import java.io.Serializable;

/**
 * Created by Gianni on 4/6/2017.
 */
public class DynamicBinaryTree<T extends Comparable> implements BinaryTree<T>, Serializable {

    private DoubleNode<T> root;

    public DynamicBinaryTree() {
        root=null;// not necessary?
    }

    public DynamicBinaryTree(T o){
        root = new DoubleNode <T>(o);
    }
    public DynamicBinaryTree(T o, DynamicBinaryTree<T> leftBranch, DynamicBinaryTree<T> rightBranch){
        root = new DoubleNode<T>(o,leftBranch.root, rightBranch.root);
    }

    public boolean isEmpty() {
        return root==null;
    }


    public DynamicBinaryTree<T> getLeft() {
        DynamicBinaryTree<T> tree = new DynamicBinaryTree<T>();
        tree.root = root.left;
        return tree;
    }


    public DynamicBinaryTree<T> getRight() {
        DynamicBinaryTree<T> tree = new DynamicBinaryTree<T>();
        tree.root = root.right;
        return tree;
    }


    public T getRoot() {
        return root.data;
    }

}
