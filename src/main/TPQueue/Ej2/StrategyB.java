package main.TPQueue.Ej2;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * class StrategyB
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/11/17.
 * <p>
 *     MainBank is used for testing purposes for Bank and all its related classes. This specific implementation
 *     if for a bank in which there is a single queue and three cashiers.
 * </p>
 */
public class StrategyB implements Strategy{

    private int peopleWaiting;
    private int interval;

    public StrategyB(int interval){
        this.interval = interval;
        peopleWaiting = 0;
    }

    /**
     * Sorts an entering batch of clients into queues.
     * @param clients The batch of clients
     * @param cashiers The bank's cashiers
     */
    public void sortClients(Client[] clients, Cashier[] cashiers){
        for (int i=0; i<clients.length; i++){
            if (clients[i].leaves(peopleWaiting));
            else fillQueue(clients[i], cashiers);
        }
        startEmptyCashiers(cashiers);
    }

    /**
     * Updates cashiers, increases time by the unit of time.
     * @param cashiers The bank's cashiers
     */
    public void updateCashiers(Cashier[] cashiers){
        peopleWaiting = cashiers[0].getQueue().size();
        int[] order = new int[cashiers.length];
        for (int i=0; i<cashiers.length; i++){
            order[i] = i;
        }
        shuffleArray(order);
        for (int i=0; i<cashiers.length; i++){
            cashiers[order[i]].nextInterval(interval);
        }
    }

    private void shuffleArray(int[] ar) {
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    public int getPeopleWaiting(){
        return peopleWaiting;
    }

    private void fillQueue(Client client, Cashier[] cashiers){
        cashiers[0].getQueue().enqueue(client);
        peopleWaiting++;
    }

    private void startEmptyCashiers(Cashier[] cashiers){
        for (int i=0; i<cashiers.length; i++){
            if (cashiers[i].isEmpty()){
                cashiers[i].start();
                if (peopleWaiting > 0){
                    peopleWaiting--;
                }
            }
        }
    }
}
