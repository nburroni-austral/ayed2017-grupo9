package main.TPSudoku;


import java.awt.event.*;
import javax.swing.*;

/**
 * class Controller
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/1/17.
 * <p>
 *     Manages "communication" between Sudoku and MainWindow.
 * </p>
 */
public class Controller {

    private MainWindow menuWindow;
    private Sudoku sudoku;
    private Timer timer;

    /**
     * Creates a Controller, which then creates MainWindow providing it ActionListeners and Sudoku.
     * The timer allows for the solution to be displayed in steps and not all at once.
     */
    public Controller(){
        menuWindow = new MainWindow(solve, reset);
        sudoku = new Sudoku();
        timer = new Timer(1 ,start);
        menuWindow.showSelf();
    }

    /**
     * ActionListeners with the behaviour that each button will follow.
     */
    ActionListener solve = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Integer[][] input = menuWindow.getUserInput();
            SudokuChecker checker = new SudokuChecker();
            if(!checker.check(input)){
                menuWindow.updateLabel("Unsolvable Sudoku.");
                return;
            }
            sudoku.setValues(input);
            start.actionPerformed(e);
        }
    };

    ActionListener start = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            timer.start();
            int x = sudoku.getX();
            int y = sudoku.getY();
            if (y<0){
                timer.stop();
                menuWindow.updateLabel("Unable to find solution.");
                return;
            }
            if (x>8){
                timer.stop();
                menuWindow.updateLabel("Solution Found!");
                return;
            }
            sudoku.solve();
            Integer[][] solution = sudoku.getSolution();
            menuWindow.displaySolution(solution);
        }

    };

    ActionListener reset = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            timer.stop();
            menuWindow.reset();
            sudoku = new Sudoku();
        }

    };

}
