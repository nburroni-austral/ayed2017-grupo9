package main.TPDistancia;

/**
 * class Main
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/16/17.
 * <p>
 *     main class used for testing.
 * </p>
 */
public class Main {
    public static void main(String[] args) {
        String word1 = "pterodactyl";
        String word2 = "psicologist";

        String word3 = "phone";
        String word4 = "iphone";

        String word5 = "castle"; //ipad and knife, calculadora and calefaccion, peter and peretla, checkers and chess, daydream and deplorable
        String word6 = "lilaco";

        System.out.println(Levenshtein.distanceCalc(word1,word2));
        System.out.println(Levenshtein.distanceCalc(word3,word4));
        System.out.println(Levenshtein.distanceCalc(word5,word6));

    }
}
