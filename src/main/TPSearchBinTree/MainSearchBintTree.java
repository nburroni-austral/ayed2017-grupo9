package main.TPSearchBinTree;

import struct.impl.BinarySearchTree;

/**
 * Created by Gianni on 4/18/2017.
 */
public class MainSearchBintTree {
    public static void main(String[] args) {

        testTree();

        bankTest();

        Ej5Test();

    }

    public static void testTree(){
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();

        tree.insert(4);
        tree.insert(9);
        tree.insert(3);
        tree.insert(1);
        tree.insert(7);

        System.out.println(tree.exists(1));
        System.out.println(tree.getMax());
        System.out.println(tree.getMin());
        System.out.println(tree.isEmpty());
    }

    public static void bankTest(){
        Ej4 bank = new Ej4();
        Account account1 = new Account(10);
        Account account2 = new Account(8);
        Account account3 = new Account(12);
        Account account4 = new Account(5);
        Account account5 = new Account(9);
        Account account6 = new Account(14);
        Account account7 = new Account(3);
        Account account8 = new Account(4);
        bank.addAccount(account1);
        bank.addAccount(account2);
        bank.addAccount(account3);
        bank.addAccount(account4);
        bank.addAccount(account5);
        bank.addAccount(account6);
        bank.addAccount(account7);
        bank.addAccount(account8);

        BinarySearchTree<Account> remove = new BinarySearchTree<>();
        remove.insert(new Account(5));
        remove.insert(new Account(3));
        remove.insert(new Account(4));

        bank.deleteAccounts(remove);
        //bank.deleteAccounts(3, 5);
    }

    public static void Ej5Test(){
        Ej5 tree = new Ej5();
        tree.insert(10);
        tree.insert(8);
        tree.insert(12);
        tree.insert(5);
        tree.insert(9);
        tree.insert(14);
        tree.insert(3);
        tree.insert(4);

        System.out.println(tree.range(3, 8));
    }
}
