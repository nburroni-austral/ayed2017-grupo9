package main.TPQueue.Ej2;

import main.TPQueue.Ej2.Cashier;
import main.TPQueue.Ej2.Client;

/**
 * interface Strategy
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/11/17
 * <p>
 *     Represents a way in which a bank can sort its clients when they enter the bank.
 * </p>
 */
public interface Strategy {

    public void sortClients(Client[] clients, Cashier[] cashiers);

    public void updateCashiers(Cashier[] cashiers);

    public int getPeopleWaiting();

}
