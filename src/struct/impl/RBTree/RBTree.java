package struct.impl.RBTree;

/**
 * Created by Dwape on 5/26/17.
 */
public class RBTree<T extends Comparable> {

    private RBNode<T> root;

    public RBTree() {
        root=null;
    }

    public RBTree(T o){
        root = new RBNode<T>(o);
    }
    public RBTree(T o, RBTree<T> leftBranch, RBTree<T> rightBranch){
        // RBNode<T> father = null; //could improve the way this works.
        root = new RBNode<T>(o,leftBranch.root, rightBranch.root, null);
    }

    public void insert(T o){
        RBNode<T> node = new RBNode<>(o);
        root = insert(root, null, node);
        checkCase(node);
    }

    private RBNode<T> insert(RBNode<T> t, RBNode<T> father, RBNode<T> node){
        if (t==null){
            t = node;
            t.father = father;
        }else if (node.data.compareTo(t.data)<0){
            t.left = insert(t.left,t, node);
        }else if(node.data.compareTo(t.data)==0){
            throw new RuntimeException("Element is already in tree");
        }else{
            t.right = insert(t.right,t, node);
        }
        return t;
    }

    private void checkCase(RBNode<T> t){
        if (t.father == null){
            t.isRed = false; //t is the root
            root = t;
            return;
        }
        if (!t.isRed){ //if node is black, nothing needs to be checked
            checkCase(t.father);
            return;
        }
        if (!t.father.isRed){
            checkCase(t.father); //Case 1: father is black.
            return;
        }
        RBNode<T> father = t.father;
        RBNode<T> grandFather = father.father;
        RBNode<T> uncle = null;
        boolean uncleIsRight = true;
        if (grandFather != null){
            if (grandFather.right != null && grandFather.right.data.compareTo(father.data) == 0){ //uncle is left
                uncle = grandFather.left; //doesn't work correctly.
                uncleIsRight = false;
            } else uncle = grandFather.right; //uncle is right
        }
        if (uncle == null || !uncle.isRed){
            boolean tIsRight = true;
            if (father.left != null && t.data.compareTo(father.left.data) == 0){  //t is left
               tIsRight = false;
            }
            if ((tIsRight && uncleIsRight) || (!tIsRight && !uncleIsRight)){ //Case 3: Black uncle, t inwards
                //double rotation
                grandFather.isRed = true;
                t.isRed = false;
                father.father = t;
                if (uncle != null){
                    uncle.father = grandFather; //is this correct?
                }
                if (uncleIsRight){
                    father.right = null;
                    if (father.left != null && father.left.father != null){
                        father.left.father = grandFather; //check
                    }
                    t.left = father;
                    t.right = grandFather;
                    grandFather.right = uncle;
                    grandFather.left = null;
                    t.father = grandFather.father; //what?????
                    if (grandFather.father != null){ //check if it works
                        if (grandFather.father.left != null && grandFather.father.left.data.compareTo(grandFather.data) == 0){
                            grandFather.father.left = t;
                        } else grandFather.father.right = t;
                    }
                } else {
                    father.left = null;
                    if (father.right != null && father.right.father != null){
                        father.right.father = grandFather; //check
                    }
                    t.right = father;
                    t.left = grandFather;
                    grandFather.left = uncle;
                    grandFather.right = null;
                    t.father = grandFather.father; //what????
                    if (grandFather.father != null){ //check if it works
                        if (grandFather.father.left != null && grandFather.father.left.data.compareTo(grandFather.data) == 0){
                            grandFather.father.left = t;
                        } else grandFather.father.right = t;
                    }
                }
                if (grandFather.father != null){
                    RBNode<T> temp = new RBNode<>(grandFather.father.data, grandFather.father.left, grandFather.father.right, grandFather.father.father);
                    t.father = temp;
                }
                grandFather.father = t;
                if (t.father != null){
                    checkCase(t.father);
                } else checkCase(t);
                return;
            } else { //Case 2: Black uncle, t outwards
                //simple rotation
                father.isRed = false;
                grandFather.isRed = true;
                father.father = grandFather.father;
                if (uncle != null){
                    uncle.father = grandFather;
                }
                if (uncleIsRight){
                    father.left = t;
                    if (father.right != null && father.right.father != null){
                        father.right.father = grandFather;
                    }
                    grandFather.left = father.right;
                    father.right = grandFather;
                    grandFather.right = uncle;
                    if (grandFather.father != null){ //not sure if it works correctly
                        if (grandFather.father.left != null && grandFather.father.left.data.compareTo(grandFather.data) == 0){
                            grandFather.father.left = father;
                        } else grandFather.father.right = father;
                    }
                } else {
                    father.right = t;
                    if (father.left != null && father.left.father != null){
                        father.left.father = grandFather;
                    }
                    //father.left.father = grandFather;
                    grandFather.right = father.left;
                    father.left = grandFather;
                    grandFather.left = uncle;
                    if (grandFather.father != null){ //not sure if it works correctly
                        if (grandFather.father.left != null && grandFather.father.left.data.compareTo(grandFather.data) == 0){
                            grandFather.father.left = father;
                        } else grandFather.father.right = father;
                    }
                }
                grandFather.father = father;
                checkCase(father);
                return;
            }
        }
        //Case 4: Father and Uncle are Red
        father.isRed = false; //turned to black
        uncle.isRed = false; //turned to black
        grandFather.isRed = true;
        checkCase(father);
    }

    public boolean isEmpty() {
        return root==null;
    }

    public RBNode<T> getRoot(){
        return root;
    }
}
