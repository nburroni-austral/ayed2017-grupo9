package main.TPStack;

import struct.impl.StaticStack;
import struct.istruct.Stack;

/**
 * class ParkingLot
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/24/2017.
 * <p>
 *     This class simulates a simple one-entry only parking lot using a Stack.
 * </p>
 */
public class ParkingLot {
    private StaticStack<Car> parking;
    private int dailyTariffs;// refreshed daily.

    public ParkingLot() {
        parking= new StaticStack<>(50);
        dailyTariffs =0;
    }

    /**
     * parkCar method
     * <p>
     *     parks a given car behind all the already parked cars
     * </p>
     * @param car the car to be parked
     */
    public void parkCar(Car car){
        if (parking.size()<50){
            parking.push(car);
        } else throw new NoMoreSpaceExc();
    }

    /**
     * unparkCar method
     * <p>
     *     unparks a given car, moving the ones behind it temporarily to the sidewalk, charges the tariff.
     * </p>
     * @param car the car to be unparked
     */
    public void unparkCar(Car car){
        Stack<Car> sidewalk = new StaticStack<>(50);
        for (int i=0;i<parking.size();i++){
            if (parking.peek().getLicensePlate()!=car.getLicensePlate()){
                sidewalk.push(parking.peek());
                parking.pop();
            }else{
                parking.pop();
                for (int j=0;j<sidewalk.size();j++){
                    parking.push(sidewalk.peek());
                    sidewalk.pop();
                }
            }
        }
        dailyTariffs +=5;
    }

    /**
     * startDay method
     * <p>
     *     method to be executed at the start of every day, it shows the amount of money earned from tariffs the
     *     previous day and rests it to zero because its the start of a new day.
     * </p>
     */
    public void startDay(){
        System.out.println(dailyTariffs);
        dailyTariffs =0;
    }
}
