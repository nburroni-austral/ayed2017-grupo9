package main.TPArbolesBinarios;

import struct.impl.DynamicBinaryTree;

import java.util.ArrayList;

/**
 * class Ej14
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/6/2017.
 * <p>
 *     Solves exercise 14 for TP7 Binary Trees.
 * </p>
 */
public class Ej14 {

    /**
     * Ej14 a) i)
     * @param tree the binary tree
     * @return returns the sum of the elements of the binary tree
     */
    public static int sum(DynamicBinaryTree<Integer> tree){
        if (tree.isEmpty()){
            return 0;
        }
        return tree.getRoot()+sum(tree.getLeft())+sum(tree.getRight());
    }

    /**
     * Ej14 a) ii)
     * @param tree the binary tree
     * @return returns the sum of the elements of the binary tree which are divisible by 3
     */
    public static int sumof3(DynamicBinaryTree<Integer> tree){
        if (tree.isEmpty()){
            return 0;
        }
        int value = 0;
        if (tree.getRoot()%3==0){
            value=tree.getRoot();
        }
        return value+sumof3(tree.getLeft())+sumof3(tree.getRight());
    }

    /**
     * Ej14 b) i)
     * @param t1 One of the trees to be compared
     * @param t2 The other tree to be compared
     * @return returns true if the trees are equal, and false if they are not.
     */
    public static boolean equal(DynamicBinaryTree t1, DynamicBinaryTree t2){
        boolean b1 =false;
        if (t1.isEmpty()&&t2.isEmpty()){
            return true;
        }else if (t1.isEmpty() || t2.isEmpty()){
            return false;
        }
        if (t1.getRoot().compareTo(t2.getRoot())==0){
            b1=true;
        }
        return b1 && equal(t1.getLeft(),t2.getLeft()) && equal(t1.getRight(),t2.getRight());
    }

    /**
     * Ej 14 b) ii)
     * <p>
     *     Checks if two trees are isomorphic, i.e. they have the same shape.
     * </p>
     * @param tree1 One of the trees
     * @param tree2 The other tree
     * @return True if the trees are isomorphic, false otherwise
     */
    public static boolean isomorphic(DynamicBinaryTree tree1, DynamicBinaryTree tree2){ //check if
        if (BinaryTreeAPI.weight(tree1) != BinaryTreeAPI.weight(tree2)) return false;
        if (BinaryTreeAPI.height(tree1) != BinaryTreeAPI.height(tree2)) return false;
        if (BinaryTreeAPI.leaves(tree1) != BinaryTreeAPI.leaves(tree2)) return false;
        for (int i=1; i <= BinaryTreeAPI.height(tree1); i++){
            if (BinaryTreeAPI.elementsInLevel(tree1, i) != BinaryTreeAPI.elementsInLevel(tree2, i)) return false;
        }
        return true;
    }

    /**
     * Ej14 b) iii)
     * <p>
     *     Two binary trees are similar if they have the same elements, even if they not have the same shape.
     * </p>
     * @param t1 One of the trees to be compared
     * @param t2 he other tree to be compared
     * @return true if the trees are similar, false if they are not
     */
    public static boolean similar(DynamicBinaryTree t1, DynamicBinaryTree t2){
        if (BinaryTreeAPI.weight(t1)!=BinaryTreeAPI.weight(t2)) return false;
        DynamicBinaryTree originalTree = t1;
        DynamicBinaryTree tree = t1;
        int i = 0;
        boolean b1 = true;
        while(!tree.isEmpty()){
            int a = BinaryTreeAPI.search(t2,tree.getRoot());
            if (a == 0){
                b1 =false;
            }
            if (i%2==0){
                tree= tree.getRight();
            }else{
                DynamicBinaryTree tempTree = originalTree.getLeft();
                originalTree = tree.getRight();
                tree = tempTree;
            }
            i++;
        }
        return b1;
    }

    /**
     * Ej 14 b) iv)
     * <p>
     *     Checks if  a tree is complete, i.e if it has exactly two "sons".
     * </p>
     * @param tree the tree to be "checked"
     * @return returns true if the tree is complete and false if it is not.
     */
    public static boolean complete(DynamicBinaryTree tree){
        if (tree.isEmpty()) return false;
        if (tree.getRight().isEmpty() && tree.getLeft().isEmpty()) return true;
        return complete(tree.getRight()) && complete(tree.getLeft());
    }

    /**
     *Ej14 b) v)
     * <p>
     *     A tree is full if it is complete and it's leaves are all in the same level
     * </p>
     * @param tree The tree to be checked
     * @return returns true if the tree is full, returns false if it is not.
     */
    public static boolean full(DynamicBinaryTree tree){
        int leaves = BinaryTreeAPI.leaves(tree);
        int height = BinaryTreeAPI.height(tree);//height == last level
        int elementsInLastLevel = BinaryTreeAPI.elementsInLevel(tree,height);
        return leaves==elementsInLastLevel && Ej14.complete(tree);
    }

    /**
     * Ej 14 b) vi)
     * <p>
     *     Checks if a tree is stable, i.e if it is empty, consisting of a single element. Or for every element of the
     *     structure his father is older.
     * </p>
     * @param tree the tree to be "checked"
     * @return returns true if the tree is stable and false if it is not
     */
    public static boolean stable(DynamicBinaryTree<Integer> tree){
        if (tree.isEmpty()) return true;
        if (tree.getRight().isEmpty() && tree.getLeft().isEmpty()) return true;
        if (tree.getRight().isEmpty()) return tree.getLeft().getRoot() < tree.getRoot();
        if (tree.getLeft().isEmpty())  return tree.getRight().getRoot() < tree.getRoot();
        if (tree.getRight().getRoot() >= tree.getRoot() || tree.getLeft().getRoot() >= tree.getRoot()) return false;
        return stable(tree.getRight()) && stable(tree.getLeft());
    }

    /**
     * Ej14 b) vii)
     * @param t1 the tree which may contain the other tree
     * @param t2 the tree which may be contained in the other
     * @return returns true if t2 occurs in t1
     */
    public static boolean occurrence(DynamicBinaryTree t1, DynamicBinaryTree t2){
        if (equal(t1,t2)) return true;
        if (t1.isEmpty()) return false;
        return occurrence(t1.getLeft(),t2) || occurrence(t1.getRight(),t2);
    }

    /**
     *Ej 14 b) viii)
     * <p>
     *     Shows the frontier of a tree, i.e the set formed by the elements stored in the leaves.
     * </p>
     * @param tree the given tree
     */
    public static void showFrontier(DynamicBinaryTree tree){
        ArrayList list = frontier(tree);
        for(int i=0; i<list.size(); i++){
            System.out.print(list.get(i)+"\t");
        }
        System.out.println("");
    }

    /**
     * Ej 14 b) ix)
     * <p>
     *     Returns the frontier of a tree, i.e the set formed by the elements stored in the leaves.
     * </p>
     * @param tree the given tree.
     * @return returns an ArrayList which represents the frontier of a tree
     */
    public static ArrayList frontier(DynamicBinaryTree tree){ //returns ArrayList
        ArrayList list = new ArrayList();
        return frontier(tree, list);
    }


    private static ArrayList frontier(DynamicBinaryTree tree, ArrayList list){
        if (tree.isEmpty()) return list;
        if (tree.getRight().isEmpty() && tree.getLeft().isEmpty()){
            list.add(tree.getRoot());
            return list;
        }
        frontier(tree.getRight(), list);
        frontier(tree.getLeft(), list);
        return list;
    }
}
