package main.TPEquipos;

import struct.impl.DynamicStack;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * class MainEquipos
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/19/2017.
 * <p>
 *     Tests the functioning of FootballTable with a series of tests
 * </p>
 */
public class MainEquipos {
    public static void main(String[] args) {

        test1();
        System.out.println("");
        test2();
        System.out.println("");
        test3();
        System.out.println("");


        test4();

    }

    /**
     * A test for 4 teams and 4 matches
     */
    private static void test1(){
        HashMap<String,Integer> scores = new HashMap<>();
        scores.put("Barcelona",4);
        scores.put("Madrid",4);
        scores.put("Sevilla",2);
        scores.put("Getafe",0);

        Match match1 = new Match("Barcelona", "Getafe");
        Match match2 = new Match("Madrid", "Sevilla");
        Match match3 = new Match("Sevilla", "Barcelona");
        Match match4 = new Match("Getafe", "Madrid");

        ArrayList<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        matches.add(match4);

        FootballTable footballTable = new FootballTable(4,4,scores,matches);
        ArrayList<DynamicStack<Character>> stacks = footballTable.solve();

        printStacks(stacks);

        /**
         * 4 4
         Barcelona 4
         Madrid 4
         Sevilla 2
         Getafe 0
         Barcelona Getafe
         Madrid Sevilla
         Sevilla Barcelona
         Getafe Madrid
         */
    }

    /**
     * A test for 6 teams and 9 matches
     */
    private static void test2(){

        HashMap<String,Integer> scores = new HashMap<>();
        scores.put("Barcelona",6);
        scores.put("Valencia",4);
        scores.put("Madrid",4);
        scores.put("Deportivo",4);
        scores.put("Sevilla",3);
        scores.put("Betis",3);

        Match match1 = new Match("Barcelona", "Deportivo");
        Match match2 = new Match("Valencia", "Deportivo");
        Match match3 = new Match("Valencia", "Barcelona");
        Match match4 = new Match("Betis", "Sevilla");
        Match match5 = new Match("Madrid", "Deportivo");
        Match match6 = new Match("Sevilla", "Madrid");
        Match match7 = new Match("Betis", "Deportivo");
        Match match8 = new Match("Madrid", "Betis");
        Match match9 = new Match("Betis", "Valencia");

        ArrayList<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        matches.add(match4);
        matches.add(match5);
        matches.add(match6);
        matches.add(match7);
        matches.add(match8);
        matches.add(match9);

        FootballTable footballTable = new FootballTable(6,9,scores,matches);
        ArrayList<DynamicStack<Character>> stacks = footballTable.solve();

        printStacks(stacks);
        /**
         * 6 9
         Barcelona 6
         Valencia 4
         Madrid 4
         Deportivo 4
         Sevilla 3
         Betis 3
         Barcelona Deportivo
         Valencia Deportivo
         Valencia Barcelona
         Betis Sevilla
         Madrid Deportivo
         Sevilla Madrid
         Betis Deportivo
         Madrid Betis
         Betis Valencia
         */

    }

    /**
     * A test for 6 teams and 10 matches
     */
    private static void test3(){
        /**
         * 6 10
         Madrid 8
         Barcelona 8
         Valencia 4
         Sevilla 4
         Deportivo 2
         Betis 0
         Madrid Deportivo
         Barcelona Sevilla
         Betis Madrid
         Betis Sevilla
         Barcelona Deportivo
         Betis Barcelona
         Madrid Barcelona
         Sevilla Deportivo
         Deportivo Valencia
         Madrid Valencia
         */

        HashMap<String,Integer> scores = new HashMap<>();
        scores.put("Madrid",8);
        scores.put("Barcelona",8);
        scores.put("Valencia",4);
        scores.put("Sevilla",4);
        scores.put("Deportivo",2);
        scores.put("Betis",0);

        Match match1 = new Match("Madrid", "Deportivo");
        Match match2 = new Match("Barcelona", "Sevilla");
        Match match3 = new Match("Betis", "Madrid");
        Match match4 = new Match("Betis", "Sevilla");
        Match match5 = new Match("Barcelona", "Deportivo");
        Match match6 = new Match("Betis", "Barcelona");
        Match match7 = new Match("Madrid", "Barcelona");
        Match match8 = new Match("Sevilla", "Deportivo");//El Clasico
        Match match9 = new Match("Deportivo", "Valencia");
        Match match10 = new Match("Madrid", "Valencia");

        ArrayList<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        matches.add(match4);
        matches.add(match5);
        matches.add(match6);
        matches.add(match7);
        matches.add(match8);
        matches.add(match9);
        matches.add(match10);

        FootballTable footballTable = new FootballTable(6,10,scores,matches);
        ArrayList<DynamicStack<Character>> stacks = footballTable.solve();

        printStacks(stacks);
    }

    /**
     * A test for 10 teams and 18 matches
     */
    private static void test4(){
        /**
         * 10 18
         Deportivo 11
         Betis 9
         Sevilla 6
         AtlMadrid 6
         Barcelona 5
         AthBilbao 4
         Madrid 2
         Espanyol 2
         Valencia 1
         RealSociedad 1
         Deportivo RealSociedad
         Barcelona AtlMadrid
         AthBilbao Espanyol
         AtlMadrid Madrid
         Deportivo Madrid
         Betis Deportivo
         RealSociedad Espanyol
         Valencia Deportivo
         Deportivo Barcelona
         Madrid Barcelona
         Espanyol Sevilla
         Sevilla AtlMadrid
         Madrid Betis
         Valencia AthBilbao
         Betis AthBilbao
         Valencia AtlMadrid
         RealSociedad Betis
         Barcelona Betis
         */

        HashMap<String,Integer> scores = new HashMap<>();
        scores.put("Deportivo",11);
        scores.put("Betis",9);
        scores.put("Sevilla",6);
        scores.put("AtlMadrid",6);
        scores.put("Barcelona",5);
        scores.put("AthBilbao",4);
        scores.put("Madrid",2);
        scores.put("Espanyol",2);
        scores.put("Valencia",1);
        scores.put("RealSociedad",1);

        Match match1 = new Match("Deportivo", "RealSociedad");
        Match match2 = new Match("Barcelona", "AtlMadrid");
        Match match3 = new Match("AthBilbao", "Espanyol");
        Match match4 = new Match("AtlMadrid", "Madrid");
        Match match5 = new Match("Deportivo", "Madrid");
        Match match6 = new Match("Betis", "Deportivo");
        Match match7 = new Match("RealSociedad", "Espanyol");
        Match match8 = new Match("Valencia", "Deportivo");
        Match match9 = new Match("Deportivo", "Barcelona");
        Match match10 = new Match("Madrid", "Barcelona");
        Match match11 = new Match("Espanyol", "Sevilla");
        Match match12 = new Match("Sevilla", "AtlMadrid");
        Match match13 = new Match("Madrid", "Betis");
        Match match14 = new Match("Valencia", "AthBilbao");
        Match match15 = new Match("Betis", "AthBilbao");
        Match match16 = new Match("Valencia", "AtlMadrid");
        Match match17 = new Match("RealSociedad", "Betis");
        Match match18 = new Match("Barcelona", "Betis");

        ArrayList<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        matches.add(match4);
        matches.add(match5);
        matches.add(match6);
        matches.add(match7);
        matches.add(match8);
        matches.add(match9);
        matches.add(match10);
        matches.add(match11);
        matches.add(match12);
        matches.add(match13);
        matches.add(match14);
        matches.add(match15);
        matches.add(match16);
        matches.add(match17);
        matches.add(match18);


        FootballTable footballTable = new FootballTable(10,18,scores,matches);
        ArrayList<DynamicStack<Character>> stacks = footballTable.solve();

        printStacks(stacks);
    }

    private static void printStacks(ArrayList<DynamicStack<Character>> stacks){
        for (int i=0; i<stacks.size();i++){
            System.out.println(stacks.get(i).peek());
        }
    }
}
