package struct.impl;

import struct.istruct.Hashable;

/**
 * Created by Dwape on 6/19/17.
 */
public class  HashTable<T extends Comparable & Hashable> {

    public DynamicList<T>[] t; //way easier to access if it is public
    private int capacity;

    public HashTable(int M)  {
        if (!Prime.isPrime(M))
            M = Prime.nextPrime(M);
        capacity = M;
        t = new DynamicList[M];
        for(int i = 0; i < M ; i++)
            t[i] = new DynamicList<>();
    }

    public void insert (T x) {
        int k = x.hashCode(capacity);
        t[k].insertNext(x); //need to check if it is inserted in the correct place.
    }

    public boolean search (T x) { //changed, check if it is ok
        int k =  x.hashCode(capacity);
        t[k].goTo(0); //go to the first position?
        int l = t[k].size();
        for (int i = 0 ; i < l ; i ++ ){
            t[k].goTo(i); //seems necessary
            if ((x.compareTo(t[k].getActual()) == 0)) return true;
        }
        return false;
    }

    public BinarySearchTree getBinarySearchTree () {
        BinarySearchTree<T> a = new BinarySearchTree<>();
        for (int i = 0; i < capacity; i++ ) {
            if (!t[i].isVoid()) { //check if it does what its supposed to do
                t[i].goTo(0); //maybe it's goTo(1);
                for (int j = 0; j < t[i].size() ; j++)   {
                    a.insert(t[i].getActual()); //check
                    t[i].goNext(); //try to improve iteration.
                    j++;
                }
            }
        }
        return a;
    }
}


