package main.TPArbolesBinarios;

import struct.impl.DynamicBinaryTree;

import java.util.ArrayList;

/**
 * class Main
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/6/2017.
 */
public class Main {
    public static void main(String[] args) {

        testSum();//4, 13 y -5

        testSumOf3();//3, 12, -6

        testEqual();//false, false, false, true

        testSimilar();//false, false, false, true

        testFull();//false, true, false, false, true

        testOccurrence();//true y false

        testIsomorphic();//false, false y true

        testComplete();//true, true, false y true

        testStable();//true, true, true y true

        testFrontier();//tree #1: 2	1	1	1, tree #2: 1	1, tree #3: 2	1

        testBinaryTreeAPI();// 7, 4, 4, 2 y 3

        testTreeTraversal();
        /**
         Array preorder: 3211211
         Array inorder: 1213121
         Array postorder: 1121123
         Array inlevels #1: 3221111
         Array inlevels #2: 311
         Array inlevels #3: 433222211111111
         */

        testWriteAndReadObject();//Done, 3211211
    }

    private static void testSum(){
        DynamicBinaryTree<Integer> treeright = new DynamicBinaryTree<>();
        DynamicBinaryTree<Integer> treeleft = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,treeleft,treeright);

        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(3,treeleft2,treeright2);

        DynamicBinaryTree<Integer> treeright3 = new DynamicBinaryTree<>(-9);
        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(3,treeleft3,treeright3);

        System.out.print("testSum: "+Ej14.sum(root)+"\t");
        System.out.print(Ej14.sum(root2)+"\t");
        System.out.println(Ej14.sum(root3));
    }

    private static void testSumOf3(){
        DynamicBinaryTree<Integer> treeright = new DynamicBinaryTree<>();
        DynamicBinaryTree<Integer> treeleft = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,treeleft,treeright);

        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(3,treeleft2,treeright2);

        DynamicBinaryTree<Integer> treeright3 = new DynamicBinaryTree<>(-9);
        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(3,treeleft3,treeright3);

        System.out.print("testSum: "+Ej14.sumof3(root)+"\t");
        System.out.print(Ej14.sumof3(root2)+"\t");
        System.out.println(Ej14.sumof3(root3));
    }

    private static void testEqual(){
        DynamicBinaryTree<Integer> treeright = new DynamicBinaryTree<>();
        DynamicBinaryTree<Integer> treeleft = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,treeleft,treeright);

        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(3,treeleft2,treeright2);


        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(3,treeleft3,root);

        DynamicBinaryTree<Integer> treeright4 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft4 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root4 = new DynamicBinaryTree<>(3,treeleft4,treeright4);

        System.out.print("testEqual: "+Ej14.equal(root,root2)+"\t");
        System.out.print(Ej14.equal(root,root3)+"\t");
        System.out.print(Ej14.equal(root2,root3)+"\t");
        System.out.println(Ej14.equal(root2,root4));
    }

    private static void testSimilar(){
        DynamicBinaryTree<Integer> treeright = new DynamicBinaryTree<>();
        DynamicBinaryTree<Integer> treeleft = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,treeleft,treeright);

        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(3);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(1,treeleft2,treeright2);


        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(3,treeleft3,root);

        DynamicBinaryTree<Integer> treeright4 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft4 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root4 = new DynamicBinaryTree<>(3,treeleft4,treeright4);

        System.out.print("testSimilar: "+Ej14.similar(root,root2)+"\t");
        System.out.print(Ej14.similar(root,root3)+"\t");
        System.out.print(Ej14.similar(root2,root3)+"\t");
        System.out.println(Ej14.similar(root2,root4));
    }

    private static void testFull(){
        DynamicBinaryTree<Integer> treeright = new DynamicBinaryTree<>();
        DynamicBinaryTree<Integer> treeleft = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,treeleft,treeright);

        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(3);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(1,treeleft2,treeright2);

        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(3,treeleft3,root);

        DynamicBinaryTree<Integer> root4 = new DynamicBinaryTree<>(3,root2,root);

        DynamicBinaryTree<Integer> root5 = new DynamicBinaryTree<>(3,root2,root2);

        System.out.print("testFull: "+Ej14.full(root)+"\t");
        System.out.print(Ej14.full(root2)+"\t");
        System.out.print(Ej14.full(root3)+"\t");
        System.out.print(Ej14.full(root4)+"\t");
        System.out.println(Ej14.full(root5));
    }

    private static void testOccurrence(){

        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(9);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(3,treeleft2,treeright2);

        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(9,treeleft3,root2);

        DynamicBinaryTree<Integer> treeright4 = new DynamicBinaryTree<>(8);
        DynamicBinaryTree<Integer> treeleft4 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root4 = new DynamicBinaryTree<>(3,treeleft4,treeright4);


        System.out.print("testOccurrence: "+Ej14.occurrence(root3,root2)+"\t");
        System.out.println(Ej14.occurrence(root3,root4));
    }

    private static void testIsomorphic(){
        DynamicBinaryTree<Integer> treeright1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root1 = new DynamicBinaryTree<>(2,treeleft1,treeright1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(2,treeleft2,treeright2);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,root1,root2);

        System.out.print("testIsomorphic: "+Ej14.isomorphic(root, root1)+"\t");
        System.out.print(Ej14.isomorphic(root, root2)+"\t");
        System.out.println(Ej14.isomorphic(root1, root2));
    }

    private static void testComplete(){
        DynamicBinaryTree<Integer> treeright1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>();
        DynamicBinaryTree<Integer> root1 = new DynamicBinaryTree<>(2,treeleft1,treeright1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(2,treeleft2,treeright2);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(2,treeleft3,treeright1);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,root1,root2);

        System.out.print("testComplete: "+Ej14.complete(root)+"\t");
        System.out.print(Ej14.complete(root1)+"\t");
        System.out.print(Ej14.complete(root3)+"\t");
        System.out.println(Ej14.complete(root2));
    }

    private static void testStable(){
        DynamicBinaryTree<Integer> treeright1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft3 = new DynamicBinaryTree<>();
        DynamicBinaryTree<Integer> root1 = new DynamicBinaryTree<>(2,treeleft1,treeright1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(2,treeleft2,treeright2);
        DynamicBinaryTree<Integer> root3 = new DynamicBinaryTree<>(2,treeleft3,treeright1);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,root1,root2);

        System.out.print("testStable: "+Ej14.stable(root)+"\t");
        System.out.print(Ej14.stable(root1)+"\t");
        System.out.print(Ej14.stable(root3)+"\t");
        System.out.println(Ej14.stable(root2));
    }

    private static void testFrontier(){
        DynamicBinaryTree<Integer> treeright1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(2);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root1 = new DynamicBinaryTree<>(2,treeleft1,treeright1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(2,treeleft2,treeright2);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,root1,root2);

        ArrayList<Integer> frontier = Ej14.frontier(root);//checked results in debugger, it works correctly
        System.out.println("testFrontier: ");
        System.out.print("\t"+"tree #1: ");
        Ej14.showFrontier(root);
        System.out.print("\t"+"tree #2: ");
        Ej14.showFrontier(root1);
        System.out.print("\t"+"tree #3: ");
        Ej14.showFrontier(root2);
    }

    private static void testBinaryTreeAPI(){
        DynamicBinaryTree<Integer> treeright1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root1 = new DynamicBinaryTree<>(2,treeleft1,treeright1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(2,treeleft2,treeright2);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,root1,root2);



        System.out.print("testBinaryTreeAPI: "+BinaryTreeAPI.weight(root)+"\t");
        System.out.print(BinaryTreeAPI.leaves(root)+"\t");
        System.out.print(BinaryTreeAPI.search(root, 1)+"\t");
        System.out.print(BinaryTreeAPI.elementsInLevel(root, 2)+"\t");
        System.out.println(BinaryTreeAPI.height(root));
    }

    private static void testTreeTraversal(){
        DynamicBinaryTree<Integer> treeright1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root1 = new DynamicBinaryTree<>(2,treeleft1,treeright1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(2,treeleft2,treeright2);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,root1,root2);

        System.out.println("testTreeTraversal: ");
        System.out.print("\t"+"Array preorder: ");
        ArrayList<Integer> array = TreeTraversal.preorder(root);
        TreeTraversal.print(array);
        System.out.print("\t"+"Array inorder: ");
        ArrayList<Integer> array2 = TreeTraversal.inorder(root);
        TreeTraversal.print(array2);
        System.out.print("\t"+"Array postorder: ");
        ArrayList<Integer> array3 = TreeTraversal.postorder(root);
        TreeTraversal.print(array3);
        ArrayList<Integer> array4 = TreeTraversal.inlevels(root);
        System.out.print("\t"+"Array inlevels #1: ");
        TreeTraversal.print(array4);

        DynamicBinaryTree<Integer> newTree = new DynamicBinaryTree<>(3,treeright1,treeleft1);
        ArrayList<Integer> array5 = TreeTraversal.inlevels(newTree);
        System.out.print("\t"+"Array inlevels #2: ");
        TreeTraversal.print(array5);

        DynamicBinaryTree<Integer> leaf8 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> leaf7 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> leaf6 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> leaf5 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> leaf4 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> leaf3 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> leaf2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> leaf1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> left12 = new DynamicBinaryTree<>(2,leaf7,leaf8);
        DynamicBinaryTree<Integer> left11 = new DynamicBinaryTree<>(2,leaf5,leaf6);
        DynamicBinaryTree<Integer> right12 = new DynamicBinaryTree<>(2,leaf3,leaf4);
        DynamicBinaryTree<Integer> right11 = new DynamicBinaryTree<>(2,leaf1,leaf2);
        DynamicBinaryTree<Integer> left1 = new DynamicBinaryTree<>(3,left11,left12);
        DynamicBinaryTree<Integer> right1 = new DynamicBinaryTree<>(3,right11,right12);
        DynamicBinaryTree<Integer> mainRoot = new DynamicBinaryTree<>(4,right1,left1);

        ArrayList<Integer> array6 = TreeTraversal.inlevels(mainRoot);
        System.out.print("\t"+"Array inlevels #3: ");
        TreeTraversal.print(array6);
    }

    private static void testWriteAndReadObject(){
        DynamicBinaryTree<Integer> treeright1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft1 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeright2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> treeleft2 = new DynamicBinaryTree<>(1);
        DynamicBinaryTree<Integer> root1 = new DynamicBinaryTree<>(2,treeleft1,treeright1);
        DynamicBinaryTree<Integer> root2 = new DynamicBinaryTree<>(2,treeleft2,treeright2);
        DynamicBinaryTree<Integer> root = new DynamicBinaryTree<>(3,root1,root2);

        System.out.println("testWriteAndReadObject: ");

        WriteObject.serializeTree(root);
        DynamicBinaryTree recoveredTree = ReadObject.deserialzeAddress("src//main//TPArbolesBinarios//Tree");

        ArrayList<Integer> list = TreeTraversal.preorder(recoveredTree);
        TreeTraversal.print(list);
    }
}
