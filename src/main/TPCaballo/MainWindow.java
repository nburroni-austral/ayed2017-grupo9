package main.TPCaballo;

import struct.impl.DynamicStack;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.Element;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * class MainWindow
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/30/17.
 * <p>
 *     Manages the graphical user interface (GUI) of HorseMovement.
 * </p>
 */
public class MainWindow extends JFrame {

    private JLabel[][] panelHolder;
    private JLabel label;
    private Image horseImg;
    private JPanel mainPanel;
    private JFormattedTextField field;

    /**
     * Creates a MainWindow.
     * @param next action listener used for next button
     * @param start action listener used for start button
     */
    public MainWindow(ActionListener next, ActionListener start){
        setTitle("Horse Movement");
        editLayout(next, start);
        try {
            Image horseTemp = ImageIO.read(getClass().getResource("./Images/HorseImg.png"));
            horseImg = horseTemp.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     *  Manages how the GUI looks.
     * @param next action listener used for next button
     * @param start action listener used for start button
     */
    private void editLayout(ActionListener next, ActionListener start){
        mainPanel = new JPanel(new BorderLayout(200,0));
        JPanel subPanel = new JPanel(new BorderLayout());
        JPanel gridPanel = new JPanel(new GridLayout(10, 10)); // 10, 10 with letters and numbers.
        JButton nextButton = new JButton("Next");
        nextButton.addActionListener(next);

        JButton startButton = new JButton("Start");
        startButton.addActionListener(start);

        JLabel jumpsLabel = new JLabel("Number of Jumps   ");

        field = new JFormattedTextField();
        field.setMaximumSize(new Dimension(155, 20));

        JLabel blank1 = new JLabel();
        blank1.setText("  ");
        JLabel blank2 = new JLabel();
        blank2.setText("  ");
        JLabel blank3 = new JLabel();
        blank3.setText("  ");

        JPanel buttonContainer = new JPanel();
        buttonContainer.setLayout(new BoxLayout(buttonContainer,BoxLayout.Y_AXIS));
        buttonContainer.add(blank1);
        buttonContainer.add(jumpsLabel);
        buttonContainer.add(field);
        buttonContainer.add(blank2);
        buttonContainer.add(startButton);
        buttonContainer.add(blank3);
        buttonContainer.add(nextButton);
        buttonContainer.setSize(new Dimension(50,50));

        panelHolder = new JLabel[8][8];

        gridPanel.add(new JLabel(""));
        for(int i=0; i<8; i++){
            int a = i+65;
            char index = (char) a;
            JLabel label = new JLabel(Character.toString(index));
            label.setHorizontalAlignment(JLabel.CENTER); //check
            gridPanel.add(label);
        }
        gridPanel.add(new JLabel(""));

        Border border = BorderFactory.createLineBorder(Color.black, 1);
        for(int m = 0; m < 8; m++) {
            JLabel numberLabel = new JLabel(Integer.toString(m+1));
            numberLabel.setHorizontalAlignment(JLabel.CENTER); //check
            gridPanel.add(numberLabel);
            JLabel newLabel = new JLabel(Integer.toString(m+1));
            newLabel.setHorizontalAlignment(JLabel.CENTER);
            gridPanel.add(newLabel);
            for(int n = 0; n < 8; n++) {
                JLabel label = new JLabel();
                label.setOpaque(true);
                if (m%2==0 && n%2!=0){
                    label.setBackground(Color.black);
                }
                if (m%2!=0 && n%2==0){
                    label.setBackground(Color.black);
                }
                label.setPreferredSize(new Dimension(50,50));
                label.setBorder(border);
                label.setHorizontalAlignment(JLabel.CENTER);
                panelHolder[m][n] = label;
                gridPanel.add(panelHolder[m][n]);
            }
            gridPanel.add(numberLabel);
        }

        gridPanel.add(new JLabel(""));
        for(int i=0; i<8; i++){
            int a = i+65;
            char index = (char) a;
            JLabel label = new JLabel(Character.toString(index));
            label.setHorizontalAlignment(JLabel.CENTER); //check
            gridPanel.add(label);
        }
        gridPanel.add(new JLabel(""));

        JPanel labelContainer = new JPanel(new FlowLayout());
        label = new JLabel();
        JLabel filler = new JLabel();
        filler.setText("  ");
        labelContainer.add(filler);
        labelContainer.add(label);

        JPanel buttonAligner = new JPanel(new BorderLayout(100,100));
        JLabel space = new JLabel("              ");
        buttonAligner.add(buttonContainer,BorderLayout.CENTER);
        buttonAligner.add(space,BorderLayout.PAGE_END);
        buttonAligner.add(space,BorderLayout.PAGE_START);

        subPanel.add(gridPanel,BorderLayout.LINE_START);
        subPanel.add(buttonAligner,BorderLayout.LINE_END);
        mainPanel.add(subPanel,BorderLayout.LINE_END);
        mainPanel.add(labelContainer,BorderLayout.PAGE_END);
        add(mainPanel);
    }

    /**
     * Displays the horse in the board.
     * @param horse the position of the horse.
     */
    public void displayBoard(Position horse){
        panelHolder[horse.getY()][horse.getX()].setIcon(new ImageIcon(horseImg));
    }

    /**
     * Displays the path the horse is following and the positions it may move to, int the form of columns.
     * @param stacks array of stacks which contain the possible movements of the horse
     */
    public void displayStacks(DynamicStack<Position>[] stacks){
        DynamicStack<Position>[] usableStacks = usableStacks(stacks);
        JPanel stacksGrid = new JPanel();
        stacksGrid.setLayout(new FlowLayout());
        for (int i=1;i<usableStacks.length;i++){
            JPanel column = new JPanel();
            column.setLayout(new BoxLayout(column,BoxLayout.Y_AXIS));
            column.setBorder(BorderFactory.createMatteBorder(0,2,2,2,Color.black));
            column.setPreferredSize(new Dimension(30,300));
            int stackSize = usableStacks[i].size();
            for (int j=0;j<stackSize;j++) {
                JLabel position = new JLabel();
                position.setText(usableStacks[i].peek().getDisplayPosition());
                usableStacks[i].pop();
                column.add(position);
            }
            stacksGrid.add(column);
        }
        JPanel stacksContainer = new JPanel(new BorderLayout(100,100));
        JLabel filler = new JLabel("              ");
        stacksContainer.add(stacksGrid,BorderLayout.CENTER);
        stacksContainer.add(filler,BorderLayout.PAGE_END);
        stacksContainer.add(filler,BorderLayout.PAGE_START);

        mainPanel.add(stacksContainer,BorderLayout.LINE_START);
    }

    private DynamicStack<Position>[] usableStacks(DynamicStack<Position>[] stacks){
        int size=0;
        for (int i=0;i<stacks.length;i++){
            if (stacks[i]!=null){
                size++;
            }
        }
        DynamicStack<Position>[] backwardsStacks = new DynamicStack[size];
        DynamicStack<Position>[] usableStacks = new DynamicStack[size];
        for (int i=0; i< size;i++){
            backwardsStacks[i] = new DynamicStack<>();
            usableStacks[i] = new DynamicStack<>();
        }
        for (int i =0; i<size;i++){
            int stackSize = stacks[i].size();
            for (int j=0;j<stackSize;j++){
                backwardsStacks[i].push(stacks[i].peek());
                stacks[i].pop();
            }
        }
        for (int i =0; i<size;i++){
            int stackSize = backwardsStacks[i].size();
            for (int j=0;j<stackSize;j++){
                usableStacks[i].push(backwardsStacks[i].peek());
                stacks[i].push(backwardsStacks[i].peek());
                backwardsStacks[i].pop();
            }
        }

        return usableStacks;
    }

    /**
     * Removes what's contained in a given position.
     * @param position the position to be eliminated
     */
    public void deletePos(Position position){
        panelHolder[position.getY()][position.getX()].setIcon(null);
    }

    /**
     *  Changes the label to display a given message.
     * @param a message to be displayed
     */
    public void updateLabel(String a){
        label.setText(a);
    }


    /**
     * @return returns the maximum amount of jumps to be done by the horse.
     */
    public int getNumberOfJumps(){
        String text = field.getText();
        Integer value = Integer.parseInt(text);
        return value;
    }
}
