package struct.impl;

/**
 * class PriorityQueue
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/9/2017.
 * <p>
 *     This class represents a priority queue, which is a data type that allows you to insert a new element and delete
 *     the "oldest" of the highest priority.
 * </p>
 */
public class PriorityQueue<T> {
    private DynamicQueue<T>[] queue;

    /**
     * Creates a priority queue
     * @param n represents the amount of different priorities.
     */
    public PriorityQueue(int n) {
        queue = new DynamicQueue[n];
        for (int i=0;i<queue.length;i++){
            queue[i] = new DynamicQueue<>();
        }
    }

    /**
     * The enqueue operation has its usual meaning, ie it is inserted as the last element of all those having the
     * same priority.
     * @param o the element to be added
     * @param priority the priority level of the element o
     */
    public void enqueue(T o, int priority){
        queue[priority-1].enqueue(o);
    }

    /**
     * The dequeue operation must eliminate and return the first of those with the highest priority.
     * @return returns the eliminated element
     */
    public T dequeue(){
        for (int i=0;i<queue.length;i++){
            if (!queue[i].isEmpty()){
                return queue[i].dequeue();
            }
        }
        throw new RuntimeException("Queue is empty");
    }

    /*
    TDA Cola de Prioridad

    Constructoras

    Nombre: PriorityQueue
    Descripción: Crea una cola de prioridad,  es un tipo de datos que permite insertar un nuevo elemento y eliminar el
    más “antiguo” de máxima prioridad.
    Dominio: Integer n que representa la cantidad de prioridades distintas
    Codominio: PriorityQueue
    Precondición: n debe pertenecer a los números naturales
    Postcondition: PriorityQueue

    Modificadoras

    Nombre: enqueue
    Descripción: La operación de insertar tiene su significado habitual, es decir, se inserta como último elemento de
    todos los que tengan la misma prioridad.
    Dominio: Elemento o; que es el elemento a ser insertado, Integer priority; qué es el nivel de prioridad que tiene
    el elemento o.
    Codominio:-.
    Precondición: o debe ser el mismo tipo de elemento que los que recibe el queue, priority debe pertenecer a los
    números naturales
    Postcondition: PriorityQueue con el elemento o insertado.

    Nombre: dequeue
    Descripción: La operación de dequeue ha de eliminar y devolver siempre el primero de entre los que tengan mayor prioridad.
    Dominio: -
    Codominio: Elemento o
    Precondición: PriorityQueue no debe estar vacía.
    Postcondition: PriorityQueue con el elemento o eliminado.
     */
}
