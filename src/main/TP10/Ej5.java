package main.TP10;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Dwape on 5/21/17.
 */
public class Ej5 {

    public static void main(String[] args) {
        String fileName = Scanner.getString("Enter the file's name: ");
        int limit = Scanner.getInt("Enter the limiting population number: ");
        String option = Scanner.getString("Enter an option: ");
        separateFile("src/main/TP10/" + fileName, limit, option);
    }

    public static void separateFile(String fileName, int limit, String option){
        try {
            FileReader fileReader = new FileReader(fileName);
            FileWriter fileWriter1 = new FileWriter("src/main/TP10/LessThan" + limit);
            FileWriter fileWriter2 = new FileWriter("src/main/TP10/MoreThan" + limit);
            int a = fileReader.read();
            while (a!=-1){
                String country = "";
                while (a!=(int)','){
                    country = country + (char)a;
                    a=fileReader.read();
                }
                a = fileReader.read();
                String population = "";
                while (a!=(int)','){
                    population = population + (char)a;
                    a=fileReader.read();
                }
                a = fileReader.read();
                String pbi = "";
                while (a!=(int)','){
                    pbi = pbi + (char)a;
                    a=fileReader.read();
                }
                a=fileReader.read();
                a=fileReader.read();
                if (Integer.parseInt(population)<limit){
                    fileWriter1.write(country+",");
                    if (option.equals("PBI")) fileWriter1.write(pbi+",\n");
                    else if (option.equals("POB")) fileWriter1.write(population+",\n");
                    else {
                        fileWriter1.write(population+",");
                        fileWriter1.write(pbi+",\n");
                    }
                }
                else {
                    fileWriter2.write(country+",");
                    if (option.equals("PBI")) fileWriter2.write(pbi+",\n");
                    else if (option.equals("POB")) fileWriter2.write(population+",\n");
                    else {
                        fileWriter2.write(population+",");
                        fileWriter2.write(pbi+",\n");
                    }
                }
            }
            fileReader.close();
            fileWriter1.close();
            fileWriter2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
