package main.TPQueue.Ej1;

import struct.impl.DynamicStack;
import struct.impl.StaticQueue;

/**
 * Created by Gianni on 4/8/2017.
 */
/**
 * class Palindrome
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/8/2017.
 */
public class Palindrome {
    /**
     * Checks if a given word is a palindrome, ie it's the same to read it from left to right than right to left.
     * @param word the word to be checked
     * @return returns true if the word is a palindrome or false if it is not.
     */
    public static boolean checkIfPalindrome(String word){
        DynamicStack<Character> stack = new DynamicStack<>();
        StaticQueue<Character> queue = new StaticQueue<>(word.length());
        for (int i=0; i<word.length();i++){
            stack.push(word.charAt(i));
            queue.inqueue(word.charAt(i));
        }
        for (int i=0; i<word.length();i++){
            if (!stack.peek().equals(queue.dequeue())) return false;
            stack.pop();
        }

        return true;
    }
}
