package main.TPSudoku;

/**
 * class Main
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  3/25/2017.
 * <p>
 *     Runs the program, instancing controller.
 * </p>
 */
public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
    }

}
