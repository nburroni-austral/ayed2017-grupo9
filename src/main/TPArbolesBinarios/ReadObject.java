package main.TPArbolesBinarios;

import struct.impl.DynamicBinaryTree;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;


/**
 * class ReadObject
 * <p>
 *     Reads a given file and recovers a DynamicBinaryTree.
 * </p>
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/8/2017.
 */
public class ReadObject {
    /**
     * Reads a file to recover a tree that was saved in it.
     * @param filename The path to the file to be read
     * @return The recovered tree
     */
    public static DynamicBinaryTree deserialzeAddress(String filename) {

        DynamicBinaryTree address = null;

        FileInputStream fin = null;
        ObjectInputStream ois = null;

        try {

            fin = new FileInputStream(filename);
            ois = new ObjectInputStream(fin);
            address = (DynamicBinaryTree) ois.readObject();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return address;

    }
}
