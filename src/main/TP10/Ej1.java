package main.TP10;

import java.io.*;

/**
 * class Ej1
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/21/2017.
 * <p>
 *     This class solves the problems posed by exercise 1 of TP10, the methods are self explanatory
 * </p>
 */
public class Ej1 {
    public static void main(String[] args) {
        String fileName = Scanner.getString("Enter file name: ");
        boolean b1=true;
        while (b1){
            System.out.println("Type C to count characters");
            System.out.println("Type L to count lines");
            System.out.println("Type any other key to exit");
            int option = Scanner.getChar("Choose an option: ");
            switch (option){
                case 'C':
                    countChars(fileName);
                    break;
                case 'L':
                    countLines(fileName);
                    break;
                default:
                    b1=false;
                    break;
            }
        }
    }

    private static void countChars(String fileName){
        try {
            FileReader fileReader = new FileReader(fileName);
            int a = fileReader.read();
            int counter=0;
            while (a!=-1){
                counter++;
                a=fileReader.read();
            }
            fileReader.close();
            System.out.println("Number of chars: "+counter);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void countLines(String fileName){
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fileReader);
            String str = br.readLine();
            int counter=0;
            while (str!=null){
                counter++;
                str=br.readLine();
            }
            System.out.println("Number of lines: "+counter);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeFileExample(){//not used
        FileWriter fr;
        String str;
        String str2;
        String str3;
        try{
            fr = new FileWriter("prog");
            str = "Why are we still here?"+"\n";
            str2 = "Just to suffer...";
            str3 = "Every night... I can feel my legs";
            fr.write(str);
            fr.write(str2);
            fr.write(str3);
            fr.close();
        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
