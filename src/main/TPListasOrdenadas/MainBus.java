package main.TPListasOrdenadas;

import struct.impl.DynamicSortedList;
import struct.impl.StaticSortedList;

/**
 * class MainBus
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  5/11/2017.
 * <p>
 *     This class tests TransportationSecretary, DynamicSortedList and StaticSortedList. It allows
 *     the use of TransportSecretary through a console menu.
 * </p>
 */
public class MainBus {

    public static void main(String[] args) {

        testStaticSortedList();

        testDynamicSortedList();
        /*
        TransportationSecretary dude = new TransportationSecretary();
        dude.addBus(new Bus(10, 11, 20, true));
        dude.addBus(new Bus(10, 12, 30, true));
        dude.addBus(new Bus(10, 9, 30, true));
        dude.addBus(new Bus(9, 13, 28, false));
        dude.disabledIntel();
        dude.seatsInLine();
        dude.print();
        dude.removeBus();
        dude.saveBuses();
        dude.retrieveBuses();
        */

        TransportationSecretary secretary = new TransportationSecretary();
        menu(secretary);
    }

    private static void testDynamicSortedList(){
        DynamicSortedList<Bus> busList = new DynamicSortedList<>();
        busList.insert(new Bus(10, 11, 20, true));
        busList.insert(new Bus(10, 12, 30, true));
        busList.insert(new Bus(10, 9, 30, true));
        busList.insert(new Bus(9, 13, 20, false));
    }

    private static void testStaticSortedList(){
        StaticSortedList<Integer> ints = new StaticSortedList<>();
        ints.insert(1);
        ints.insert(2);
        ints.insert(3);
        ints.insert(5);
        ints.insert(4);
        ints.insert(20);
        ints.insert(7);
        ints.insert(9);
        ints.insert(8);
        ints.insert(6);

        for (int i=0; i<ints.size();i++){
            ints.goTo(i);
            //System.out.println(ints.getActual());
        }
    }

    private static void menu(TransportationSecretary secretary){
        boolean loop = true;
        while (loop){
            System.out.println("");
            System.out.println("1.Add Bus");
            System.out.println("2.Remove Bus");
            System.out.println("3.Obtain an ordered Report");
            System.out.println("4.Amount of disabled friendly buses");
            System.out.println("5.Amount of buses with more than 27 seats");
            System.out.println("6.Save buses");
            System.out.println("7.Load buses");
            System.out.println("8. Exit");
            int input = Scanner.getInt("Choose an option: ");
            System.out.println("");
            switch (input) {
                case 1: int lineNumber = Scanner.getInt("Enter bus line: ");
                    int internalNumber = Scanner.getInt("Enter internal number: ");
                    int numberOfSeats = Scanner.getInt("Enter number of seats: ");
                    int friendly = Scanner.getInt("Is is disabled friendly? (1 yes, 0 no): ");
                    boolean isDisabledFriendly = friendly == 1;
                    Bus bus = new Bus(lineNumber, internalNumber, numberOfSeats, isDisabledFriendly);
                    secretary.addBus(bus);
                    System.out.println("Bus created successfully!");
                    break;
                case 2: int line = Scanner.getInt("Enter bus line: ");
                    int internal = Scanner.getInt("Enter internal number: ");
                    secretary.removeBus(line, internal);
                    break;
                case 3:
                    System.out.println("Report: ");
                    secretary.print();
                    break;
                case 4: secretary.disabledIntel();
                    break;
                case 5: secretary.seatsInLine();
                    break;
                case 6: secretary.saveBuses();
                    System.out.println("Buses saved successfully!");
                    break;
                case 7: secretary.retrieveBuses();
                    System.out.println("Buses loaded successfully!");
                    break;
                case 8: loop = false;
                    break;
                default: break;
            }
        }
    }
}
