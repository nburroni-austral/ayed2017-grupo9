package main.TP10;

import main.TPListasOrdenadas.*;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Dwape on 5/21/17.
 */
public class Ej3 {

    public static void main(String[] args) {
        String from = Scanner.getString("Enter the file you'd like to use as preset: ");
        String to = Scanner.getString("Enter the file where the text will be copied to: ");
        int option = Scanner.getInt("Upper or Lower case? (0 Lower, Upper otherwise): ");
        copyToLowerOrUpperCase(from, to, option);
    }

    public static void copyToLowerOrUpperCase(String from, String to, int a){
        FileReader fileReader;
        FileWriter fileWriter;
        try{
            fileReader = new FileReader("src/main/TP10/" + from); //file path could differ
            fileWriter = new FileWriter("src/main/TP10/" + to, true);
            int b = 0;
            if (a==0){
                while(b!=-1){
                    b = fileReader.read();
                    fileWriter.write(Character.toLowerCase(b));
                }
            } else{
                while(b!=-1){
                    b = fileReader.read();
                    fileWriter.write(Character.toUpperCase(b));
                }
            }
            fileWriter.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
