package main.TPArbolesBinarios;

import struct.impl.DynamicBinaryTree;

/**
 * class BinaryTreeAPI
 * @author Gianluca Scolaro
 * @author Eduardo Lalor
 * @since  4/6/2017.
 * <p>
 *     This class solves the problems posed by exercise 13
 * </p>
 */
public class BinaryTreeAPI {

    /**
     * Ej 13 a)
     * @param a the given tree
     * @return returns an int whose value represents the weight of a given tree.
     */
    public static int weight(DynamicBinaryTree a){
        if(a.isEmpty()) return 0;
        else return 1+weight(a.getLeft())+weight(a.getRight());
    }

    /**
     * Ej 13 b)
     * @param a the given tree
     * @return returns an int whose value represents the amount of leaves of a given tree.
     */
    public static int leaves(DynamicBinaryTree a){
        if(a.isEmpty()) return 0;
        if(a.getLeft().isEmpty() && a.getRight().isEmpty()) return 1;
        return leaves(a.getLeft()) + leaves(a.getRight());
    }

    /**
     *Ej 13 c)
     * @param a the tree where the element may be.
     * @param element the element to be searched for.
     * @return returns an int whose value represents the amount of leaves of a given tree.
     */
    public static int search(DynamicBinaryTree a, Object element){
        if (a.isEmpty()) return 0;
        if (a.getLeft().isEmpty() && a.getRight().isEmpty()){
            if(a.getRoot().equals(element)) return 1;
            return 0;
        }
        else{
            if(a.getRoot().equals(element)) return 1+search(a.getLeft(), element)+search(a.getRight(), element);
            return search(a.getLeft(), element)+search(a.getRight(), element);
        }
    }

    /**
     * Ej 13 d)
     * @param a the given tree
     * @param level the given level
     * @return returns an int whose value represents the amount of elements a tree has in a given level.
     */
    public static int elementsInLevel(DynamicBinaryTree a, int level){
        if (level==1){
            if (a.isEmpty()) return 0;
            return 1;
        }
        return elementsInLevel(a.getRight(), level-1) + elementsInLevel(a.getLeft(), level-1);
    }

    /**
     * Ej 13 e)
     * @param a the given tree
     * @return returns an int whose value represents height of the given tree.
     */
    public static int height(DynamicBinaryTree a){
        if(a.isEmpty()) return 0;
        if(a.getLeft().isEmpty() && a.getRight().isEmpty()) return 1;
        return 1+Math.max(height(a.getLeft()), height(a.getRight()));
    }
}
