package main.TPSearchBinTree;

/**
 * Created by Gianni on 4/19/2017.
 */
public class Account implements Comparable<Account> {
    private int code;

    public Account(int code) {
        this.code = code;
    }

    @Override
    public int compareTo(Account o) {
        return code-o.getCode();
    }

    public int getCode() {
        return code;
    }
}
